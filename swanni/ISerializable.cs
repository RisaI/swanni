﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
namespace swanni
{
	public interface ISerializable
	{
		void Serialize(System.IO.BinaryWriter writer);
		void Deserialize(System.IO.BinaryReader reader);
	}

	[AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
	public class SerializationAttribute : Attribute
	{
		public string Identifier;

		public SerializationAttribute(string identifier)
		{
			Identifier = identifier;
		}
	}

	public static class DeserializationRegister
	{
		static Dictionary<string, Func<object>> Register = new Dictionary<string, Func<object>>();

		/// <summary>
		/// Prida vsechny kvalifikovane tridy (s tagem [Serialization]) dane Assembly do registru.
		/// </summary>
		/// <param name="assembly">Assembly.</param>
		public static void ScanAssembly(System.Reflection.Assembly assembly)
		{
			//Projde vsechny tridy v dane Assembly a tridy, ktere maji atribut [Serialization] prida do registru.
			foreach (Type t in assembly.GetTypes())
			{
				var l = t.GetCustomAttributes(typeof(SerializationAttribute), false);
				if (l.Length > 0)
				{
					AddToRegister(((SerializationAttribute)l[0]).Identifier, t);
				}
			}
		}

		/// <summary>
		/// Prida typ do deserializacniho registru pod danym identifikatorem.
		/// </summary>
		/// <param name="identifier">Identifikator.</param>
		/// <param name="t">Typ.</param>
		public static void AddToRegister(string identifier, Type t)
		{
			if (!Register.ContainsKey(identifier) && t.GetConstructor(Type.EmptyTypes) != null)
			{
				Register.Add(identifier, Expression.Lambda<Func<object>>(Expression.New(t.GetConstructor(Type.EmptyTypes))).Compile());
			}
		}

		/// <summary>
		/// Vytvori instanci typu daneho identifikatoru.
		/// </summary>
		/// <returns>Instance.</returns>
		/// <param name="identifier">Identifikator.</param>
		public static object GetInstance(string identifier)
		{
			if (Register.ContainsKey(identifier))
				return Register[identifier]();

			return null;
		}

		public static string DebugString()
		{
			string str = string.Empty;
			Register.Keys.ToList().ForEach(k => { str += k + " "; });
			return str;
		}
	}
}
