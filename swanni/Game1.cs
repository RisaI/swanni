﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using swanni.Game;

namespace swanni
{
	/// <summary>
	/// This is the main type for your game.
	/// </summary>
	public class Game1 : Microsoft.Xna.Framework.Game
	{
		public static string ProjectName
		{
			get { return "swanni"; }
		}

		public static string Version
		{
			get { return "Early Development (pre-dev)"; }
		}

		/// <summary>
		/// Promenna urcuje soucasny herni stav (menu/hra...)
		/// </summary>
		private GameStateEnum _gameState = GameStateEnum.Game;
		/// <summary>
		/// Nastavi nebo vrati aktualni herni stav.
		/// </summary>
		/// <value>Herni stav.</value>
		public GameStateEnum GameState
		{
			get { return _gameState; }
			set
			{
				//Loading statu
				switch (value)
				{
					case GameStateEnum.Editor:
						Editor.Initialize(this);
						break;
					case GameStateEnum.Intro:
						_introTimer = 0;
						break;
					case GameStateEnum.Game:
						world = new GameWorld(this);
						//world.LoadTilemap();
						Console.ControlledWorld = world;
						break;
				}
				_gameState = value;
			}
		}

		public Vector2 Resolution
		{
			get { return new Vector2(graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight); }
		}

		public DeveloperConsole Console
		{
			get;
			private set;
		}

		GraphicsDeviceManager graphics;
		SpriteBatch spriteBatch;

		public Game1()
		{
			graphics = new GraphicsDeviceManager(this);
			Content.RootDirectory = "Content";
			IsMouseVisible = true;
			Console = new DeveloperConsole(this);
			DeserializationRegister.ScanAssembly(GetType().Assembly); //Zaregistruje tridy k deserializaci.
		}

		/// <summary>
		/// Allows the game to perform any initialization it needs to before starting to run.
		/// This is where it can query for any required services and load any non-graphic
		/// related content.  Calling base.Initialize will enumerate through any components
		/// and initialize them as well.
		/// </summary>
		protected override void Initialize()
		{
			Window.ClientSizeChanged += (sender, e) =>
			{
				graphics.PreferredBackBufferWidth = Window.ClientBounds.Width;
				graphics.PreferredBackBufferHeight = Window.ClientBounds.Height;
			};
			base.Initialize();
		}

		/// <summary>
		/// LoadContent will be called once per game and is the place to load
		/// all of your content.
		/// </summary>
		protected override void LoadContent()
		{
			// Create a new SpriteBatch, which can be used to draw textures.
			spriteBatch = new SpriteBatch(GraphicsDevice);

			//Nacte content
			ContentLoader.Load(this);

			//Nastavit gamestate na hru (v ramci debugu)
			GameState = GameStateEnum.Editor;
		}

		/// <summary>
		/// UnloadContent will be called once per game and is the place to unload
		/// game-specific content.
		/// </summary>
		protected override void UnloadContent()
		{
			
		}

		private GameWorld world;

		/// <summary>
		/// Allows the game to run logic such as updating the world,
		/// checking for collisions, gathering input, and playing audio.
		/// </summary>
		/// <param name="gameTime">Provides a snapshot of timing values.</param>
		protected override void Update(GameTime gameTime)
		{
			Input.RefreshInput();
			Console.Update(gameTime);

			switch (_gameState)
			{
				case GameStateEnum.Intro:
					UpdateIntro(gameTime);
					break;
				case GameStateEnum.Menu:
					//zpracovat menu (kliky, nastaveni...)
					break;
				case GameStateEnum.Editor:
					if (!Console.Enabled)
						Editor.Update(gameTime);
					break;
				case GameStateEnum.Game:
					//herni logika
					if (world != null && !Console.Enabled)
						world.Update(gameTime);
					break;
			}

			base.Update(gameTime);
		}

		/// <summary>
		/// This is called when the game should draw itself.
		/// </summary>
		/// <param name="gameTime">Provides a snapshot of timing values.</param>
		protected override void Draw(GameTime gameTime)
		{
			GraphicsDevice.Clear(Color.CornflowerBlue);

			switch (_gameState)
			{
				case GameStateEnum.Intro:
					DrawIntro();
					break;
				case GameStateEnum.Menu:
					//vykreslit menu
					break;
				case GameStateEnum.Editor:
					Editor.Draw(spriteBatch);
					break;
				case GameStateEnum.Game:
					//vykreslit hru
					if (world != null)
						world.Draw(spriteBatch);
					break;
			}

			if (Console.Enabled)
				Console.Draw(spriteBatch);
			base.Draw(gameTime);
		}

		#region Intro
		const float INTRO_TIME = 5f, INTRO_FADETIME = 1f;
		private float _introTimer; // Pocitadlo delky prehravani

		/// <summary>
		/// Updatuje intro obrazovku.
		/// </summary>
		/// <param name="gameTime">Game time.</param>
		private void UpdateIntro(GameTime gameTime)
		{
			_introTimer += (float)gameTime.ElapsedGameTime.TotalSeconds;

			if (_introTimer >= INTRO_TIME || Input.KeyPressed(Keys.Space))
				GameState = GameStateEnum.Game;
		}

		/// <summary>
		/// Vykresli intro obrazovku.
		/// </summary>
		private void DrawIntro()
		{
			string text = "INTRO TEXT";
			var font = ContentLoader.Fonts["console"];
			float scale = 1;
			if (_introTimer <= INTRO_FADETIME)
				scale = (float)Math.Sin(MathHelper.PiOver2 * _introTimer / INTRO_FADETIME);
			else if (_introTimer >= INTRO_TIME - INTRO_FADETIME)
				scale = (float)-Math.Sin(MathHelper.PiOver2 * (_introTimer - INTRO_TIME) / INTRO_FADETIME);
			Color color = Color.White * scale;
			spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend);
			spriteBatch.DrawString(font, text, Resolution / 2, color, 0, font.MeasureString(text) / 2, 1, SpriteEffects.None, 0f);
			spriteBatch.End();
		}
#endregion
	}

	//Enumerace moznych hernich stavu
	public enum GameStateEnum
	{
		Intro,
		Menu,
		Loading,
		Editor,
		Game
	}
}
