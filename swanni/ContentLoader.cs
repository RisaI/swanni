﻿using System;
using System.IO;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Newtonsoft.Json.Linq;
namespace swanni
{
	static class ContentLoader
	{
		public static Texture2D Pixel
		{
			get;
			private set;
		}

		public static Dictionary<string, Texture2D> Textures;
		public static List<Texture2D> Tilesets;
		public static Dictionary<string, SpriteFont> Fonts;

		/// <summary>
		/// Kolekce Sprite instanci, neni public, pristup pouze prez GetSprite metodu.
		/// </summary>
		static Dictionary<string, Sprite> Sprites;
		static Dictionary<string, Game.Entity> Entities;
		public static Dictionary<string, Game.GameWorld.Location> Locations;

		static ContentManager content;
		public static void Load(Game1 game)
		{
			//Nastavi content referenci na ContentManager Game1 instance.
			content = game.Content;

			//Vygenerovat pixel
            Pixel = new Texture2D(game.GraphicsDevice, 1, 1);
			Pixel.SetData<Color>(new Color[] { Color.White });

			//Nacist fonty
			Fonts = new Dictionary<string, SpriteFont>(1);
			LoadFont("console");

			//Nacist tilesety
			Tilesets = new List<Texture2D>();
			Tilesets.Add(game.Content.Load<Texture2D>("textures/tileset0"));

			//Nacist textury
			Textures = new Dictionary<string, Texture2D>(3);
			LoadTexture("character");

			//Nacist sprity
			Sprites = new Dictionary<string, Sprite>(16);
			foreach (string file in FindAllFiles("Assets/sprites", ".sprite"))
			{
				var name = Path.GetFileNameWithoutExtension(file);
				Console.WriteLine("Loading sprite: {0}", name);
				using (FileStream stream = new FileStream(file, FileMode.Open))
				using (StreamReader sReader = new StreamReader(stream))
				using (Newtonsoft.Json.JsonTextReader reader = new Newtonsoft.Json.JsonTextReader(sReader))
				{
					Sprites.Add(name, new Sprite(JObject.Load(reader)));
				}
			}

			//Nacist entity
			Entities = new Dictionary<string, Game.Entity>(16);
			foreach (string file in FindAllFiles("Assets/entities", ".entity"))
			{
				var name = Path.GetFileNameWithoutExtension(file);
				Console.WriteLine("Loading entity: {0}", name);
				using (FileStream stream = new FileStream(file, FileMode.Open))
				using (StreamReader sReader = new StreamReader(stream))
				using (Newtonsoft.Json.JsonTextReader reader = new Newtonsoft.Json.JsonTextReader(sReader))
				{
					var ent = new Game.Entity();
					ent.ReadFromJson(JObject.Load(reader));
					ent.AssetName = name;
					Entities.Add(name, ent);
				}
			}

			//Nacist lokace
			Locations = new Dictionary<string, Game.GameWorld.Location>();
			foreach (string file in FindAllFiles("Assets/locations", ".location"))
			{
				var name = Path.GetFileNameWithoutExtension(file);
				Console.WriteLine("Loading location: {0}", name);
				Locations.Add(name, Game.GameWorld.Location.LoadFromFile(name, file));
			}
		}

		/// <summary>
		/// Rekurzivne vyhleda vsechny soubory s priponou ve slozce a podslozkach.
		/// </summary>
		/// <returns>Seznam souboru.</returns>
		/// <param name="path">Cesta.</param>
		/// <param name="extension">Pripona.</param>
		private static List<string> FindAllFiles(string path, string extension)
		{
			var list = new List<string>();
			if (!Directory.Exists(path))
				return list;
			
			foreach (string file in Directory.GetFiles(path))
			{
				if (Path.GetExtension(file) == extension)
					list.Add(file);
			}
			foreach (string subdir in Directory.GetDirectories(path))
			{
				list.AddRange(FindAllFiles(subdir, extension));
			}
			return list;
		}

		/// <summary>
		/// Vrati klon instance nacteneho sprite.
		/// </summary>
		/// <returns>Sprite.</returns>
		/// <param name="name">Nazev.</param>
		public static Sprite GetSprite(string name)
		{
			return Sprites[name].Clone();
		}

		/// <summary>
		/// Vrati klon instance nactene entity.
		/// </summary>
		/// <returns>Entita.</returns>
		/// <param name="name">Nazev.</param>
		public static Game.Entity GetEntity(string name)
		{
			return Entities[name].Clone();
		}

		/// <summary>
		/// Zjisti, jestli je dany entity asset unikatni.
		/// </summary>
		/// <returns>Je entita unikatni?</returns>
		/// <param name="name">Nazev.</param>
		public static bool IsEntityUnique(string name)
		{
			return Entities[name].Unique;
		}

		/// <summary>
		/// Nacte texturu a ulozi ji do Dictionary pod jejim asset nazvem.
		/// </summary>
		/// <param name="asset">Asset nazev v originalni Content/textures/ slozce.</param>
        static void LoadTexture(string asset)
		{
			Textures.Add(asset, content.Load<Texture2D>("textures/" + asset));
		}

		/// <summary>
		/// Nacte texturu a ulozi ji do Dictionary pod libovolnym nazvem.
		/// </summary>
		/// <param name="fullAsset">Plna cesta k assetu.</param>
		/// <param name="dictKey">Klic, pod kterym bude textura ulozena.</param>
		static void LoadTexture(string fullAsset, string dictKey)
		{
			Textures.Add(dictKey, content.Load<Texture2D>(fullAsset));
		}

		/// <summary>
		/// Nacte font a ulozi jej do Dictionary pod jeho asset nazvem.
		/// </summary>
		/// <param name="asset">Asset nazev.</param>
		static void LoadFont(string asset)
		{
			Fonts.Add(asset, content.Load<SpriteFont>("fonts/" + asset));
		}

		/// <summary>
		/// Prevede index na pozici v tilesetu.
		/// </summary>
		/// <returns>Pozice v tilesetu.</returns>
		/// <param name="tileset">Tileset.</param>
		/// <param name="id">Index.</param>
		public static Rectangle IdToTilesetPosition(byte tileset, UInt16 id)
		{
			switch (tileset)
			{
				case 0:
					return new Rectangle((id % 24) * 16, (id / 24) * 16, 16, 16);
			}
			return default(Rectangle);
		}
	}
}
