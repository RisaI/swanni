﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

namespace swanni
{
	public static class Editor
	{
		const float CAMERA_SPEED = 600f;

		public static Tile PLACING = new Tile() { ID = 5 };
		public static bool PLAYING = false;

		static Game.GameWorld _world;
		static Game1 _game;

		public static void Initialize(Game1 game)
		{
			_game = game;
			_world = new Game.GameWorld(_game);
			game.Console.ControlledWorld = _world;
			_world.CreateLocation("editor", 4, 4);
			PLAYING = false;
		}

		public static void Update(GameTime gameTime)
		{
			if (Input.KeyPressed(Keys.F5))
				PLAYING = !PLAYING;

			if (PLAYING)
			{
				_world.Update(gameTime);
			}
			else
			{
				if (Input.Binds["up"].IsDown())
					_world.ActiveCamera.Position.Y -= CAMERA_SPEED * (float)gameTime.ElapsedGameTime.TotalSeconds;
				if (Input.Binds["down"].IsDown())
					_world.ActiveCamera.Position.Y += CAMERA_SPEED * (float)gameTime.ElapsedGameTime.TotalSeconds;
				if (Input.Binds["left"].IsDown())
					_world.ActiveCamera.Position.X -= CAMERA_SPEED * (float)gameTime.ElapsedGameTime.TotalSeconds;
				if (Input.Binds["right"].IsDown())
					_world.ActiveCamera.Position.X += CAMERA_SPEED * (float)gameTime.ElapsedGameTime.TotalSeconds;

				if (Input.MState.LeftButton == ButtonState.Pressed)
				{
					_world.SetTileOnPosition_P(_world.ActiveCamera.ScreenToWorld(Input.MousePosition), PLACING.Tileset, PLACING.ID, PLACING.Collision);
				}
			}
		}

		public static void Draw(SpriteBatch spriteBatch)
		{
			_world.Draw(spriteBatch, (byte)(PLAYING ? 0 : 3));

			var font = ContentLoader.Fonts["console"];
			spriteBatch.Begin();
			spriteBatch.DrawString(font, "Placing tile: " + PLACING.Tileset + ", " + PLACING.ID + ", " + PLACING.Collision, Vector2.Zero, Color.White);
			spriteBatch.End();
		}
	}
}
