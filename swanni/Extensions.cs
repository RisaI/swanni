﻿using System;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Newtonsoft.Json;
namespace swanni
{
	public static class Extensions
	{
		#region SpriteBatch
		/// <summary>
		/// Vykresli obdelnik pomoci tridy SpriteBatch.
		/// </summary>
		/// <param name="spriteBatch">Sprite batch.</param>
		/// <param name="area">Plocha.</param>
		/// <param name="color">Barva.</param>
		/// <param name="depth">SpriteBatch hloubka.</param>
		public static void DrawRectangle(this SpriteBatch spriteBatch, Rectangle area, Color color, float depth)
		{
			spriteBatch.Draw(ContentLoader.Pixel, area, null, color, 0, Vector2.Zero, SpriteEffects.None, depth);
		}
		#endregion

		#region BinaryWriter/Reader
		/// <summary>
		/// Serializuje instanci tridy, ktera implementuje ISerializable rozhrani.
		/// </summary>
		/// <param name="writer">Writer.</param>
		/// <param name="toSerialize">Objekt k serializaci.</param>
		public static void Write(this System.IO.BinaryWriter writer, ISerializable toSerialize)
		{
			toSerialize.Serialize(writer);
		}

		/// <summary>
		/// Serializuje GUID.
		/// </summary>
		/// <param name="writer">Writer.</param>
		/// <param name="guid">GUID.</param>
		public static void Write(this System.IO.BinaryWriter writer, Guid guid)
		{
			writer.Write(guid.ToByteArray());
		}

		/// <summary>
		/// Serializuje Vector2.
		/// </summary>
		/// <param name="writer">Writer.</param>
		/// <param name="vector">Vector2.</param>
		public static void Write(this System.IO.BinaryWriter writer, Vector2 vector)
		{
			writer.Write(vector.X);
			writer.Write(vector.Y);
		}

		/// <summary>
		/// Serializuje barvu.
		/// </summary>
		/// <param name="writer">Writer.</param>
		/// <param name="color">Barva.</param>
		public static void Write(this System.IO.BinaryWriter writer, Color color)
		{
			writer.Write(color.R);
			writer.Write(color.G);
			writer.Write(color.B);
			writer.Write(color.A);
		}

		/// <summary>
		/// Precte GUID.
		/// </summary>
		/// <returns>GUID.</returns>
		/// <param name="reader">Reader.</param>
		public static Guid ReadGuid(this System.IO.BinaryReader reader)
		{
			return new Guid(reader.ReadBytes(16));
		}

		/// <summary>
		/// Precte Vector2.
		/// </summary>
		/// <returns>Vector2.</returns>
		/// <param name="reader">Reader.</param>
		public static Vector2 ReadVector2(this System.IO.BinaryReader reader)
		{
			return new Vector2(reader.ReadSingle(), reader.ReadSingle());
		}

		/// <summary>
		/// Precte barvu.
		/// </summary>
		/// <returns>Barva.</returns>
		/// <param name="reader">Reader.</param>
		public static Color ReadColor(this System.IO.BinaryReader reader)
		{
			return new Color(reader.ReadByte(), reader.ReadByte(), reader.ReadByte(), reader.ReadByte());
		}
		#endregion

		#region Json
		/// <summary>
		/// Serializuje Vector2 do JSON formatu.
		/// </summary>
		/// <param name="writer">Writer.</param>
		/// <param name="vector">Vector2.</param>
		public static void WriteVector2(this JsonWriter writer, Vector2 vector)
		{
			writer.WriteValue(vector.X + ";" + vector.Y);
		}

		/// <summary>
		/// Serializuje barvu.
		/// </summary>
		/// <param name="writer">Writer.</param>
		/// <param name="color">Barva.</param>
		public static void WriteColor(this JsonWriter writer, Color color)
		{
			writer.WriteValue(color.R + ";" + color.G + ";" + color.B + ";" + color.A);
		}
		#endregion
	}
}
