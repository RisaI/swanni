﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
namespace swanni
{
	public static class Input
	{
		public static KeyboardState PrevKBState, KBState;
		public static MouseState PrevMState, MState;
		public static Dictionary<string, InputBind> Binds;

		static Input()
		{
			Binds = new Dictionary<string, InputBind>();
			Binds.Add("up", new KeyBind(Keys.W));
			Binds.Add("down", new KeyBind(Keys.S));
			Binds.Add("left", new KeyBind(Keys.A));
			Binds.Add("right", new KeyBind(Keys.D));
		}

		/// <summary>
		/// Refreshne input.
		/// </summary>
		public static void RefreshInput()
		{
			PrevKBState = KBState;
			KBState = Keyboard.GetState();
			PrevMState = MState;
			MState = Mouse.GetState();
		}

		/// <summary>
		/// Byla klavesa stisknuta?
		/// </summary>
		/// <returns><c>true</c>, pokud byla stistknuta, <c>false</c> v opacnem pripade.</returns>
		/// <param name="key">Klavesa.</param>
		public static bool KeyPressed(Keys key)
		{
			return PrevKBState.IsKeyUp(key) && KBState.IsKeyDown(key);
		}

		/// <summary>
		/// Byla klavesa pustena?
		/// </summary>
		/// <returns><c>true</c>, pokud byla stisknuta, <c>false</c> v opacnem pripade.</returns>
		/// <param name="key">Klavesa.</param>
		public static bool KeyReleased(Keys key)
		{
			return KBState.IsKeyUp(key) && PrevKBState.IsKeyDown(key);
		}

		public static Vector2 MousePosition
		{
			get { return new Vector2(MState.X, MState.Y); }
		}

		public abstract class InputBind
		{
			public abstract bool IsPressed();
			public abstract bool IsReleased();
			public abstract bool IsDown();
			public abstract bool IsUp();
		}

		public class KeyBind : InputBind
		{
			private Keys _key;
			public KeyBind(Keys key)
			{
				_key = key;
			}

			public override bool IsDown()
			{
				return KBState.IsKeyDown(_key);
			}

			public override bool IsUp()
			{
				return !IsDown();
			}

			public override bool IsPressed()
			{
				return KeyPressed(_key);
			}

			public override bool IsReleased()
			{
				return KeyReleased(_key);
			}
		}

		public class MouseBind : InputBind
		{
			private MouseButton _button;
			public MouseBind(MouseButton btn)
			{
				_button = btn;
			}

			public override bool IsDown()
			{
				switch (_button)
				{
					case MouseButton.Left:
						return MState.LeftButton == ButtonState.Pressed;
					case MouseButton.Right:
						return MState.RightButton == ButtonState.Pressed;
					case MouseButton.Middle:
						return MState.MiddleButton == ButtonState.Pressed;
				}
				return false;
			}

			public override bool IsUp()
			{
				return !IsDown();
			}

			public override bool IsPressed()
			{
				switch (_button)
				{
					case MouseButton.Left:
						return MState.LeftButton == ButtonState.Pressed && PrevMState.LeftButton == ButtonState.Released;
					case MouseButton.Right:
						return MState.RightButton == ButtonState.Pressed && PrevMState.RightButton == ButtonState.Released;
					case MouseButton.Middle:
						return MState.MiddleButton == ButtonState.Pressed && PrevMState.MiddleButton == ButtonState.Released;
				}
				return false;
			}

			public override bool IsReleased()
			{
				switch (_button)
				{
					case MouseButton.Left:
						return MState.LeftButton == ButtonState.Released && PrevMState.LeftButton == ButtonState.Pressed;
					case MouseButton.Right:
						return MState.RightButton == ButtonState.Released && PrevMState.RightButton == ButtonState.Pressed;
					case MouseButton.Middle:
						return MState.MiddleButton == ButtonState.Released && PrevMState.MiddleButton == ButtonState.Pressed;
				}
				return false;
			}

			public enum MouseButton
			{
				Left,
				Right,
				Middle
			}
		}
	}
}