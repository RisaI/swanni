﻿using System;
using System.Globalization;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
namespace swanni
{
	public static class Helper
	{
		/// <summary>
		/// Prevede uhel na smerovy vektor, podle standardni konvence (uhel 0 ukazuje vpravo).
		/// </summary>
		/// <returns>Smerovy vektor (velikost 1).</returns>
		/// <param name="angle">Uhel.</param>
        public static Vector2 AngleToDirection(float angle)
		{
			return new Vector2((float)Math.Cos(angle), (float)-Math.Sin(angle));
		}

		/// <summary>
		/// Prevede smerovy vektor na uhel.
		/// </summary>
		/// <returns>Uhel.</returns>
		/// <param name="dir">Vektor.</param>
		public static float DirectionToAngle(Vector2 dir)
		{
			return (float)Math.Atan2(-dir.Y, dir.X);
		}

		/// <summary>
		/// Vrati velikost textury jako Vector2.
		/// </summary>
		/// <returns>Velikost textury.</returns>
		/// <param name="texture">Textura.</param>
		public static Vector2 Size(this Texture2D texture)
		{
			return new Vector2(texture.Width, texture.Height);
		}

		/// <summary>
		/// Vrati ID smeru (0 vpravo, 1 nahoru, 2 vlevo, 3 dolu)
		/// </summary>
		/// <returns>ID smeru.</returns>
		/// <param name="direction">Smer.</param>
		public static int DirectionId(Vector2 direction)
		{
			var angle = DirectionToAngle(direction);
			if (Math.Abs(angle) > MathHelper.PiOver4 * 3)
				return 2;
			if (Math.Abs(angle) < MathHelper.PiOver4)
				return 0;
			if (angle > 0)
				return 1;
			return 3;
		}

		/// <summary>
		/// Zaokrouhluje zaporne integery opacnym smerem.
		/// </summary>
		/// <returns>Vysledek deleni.</returns>
		/// <param name="a">Delenec.</param>
		/// <param name="b">Delitel.</param>
		public static int NegDivision(int a, int b)
		{
			return a < 0 ? a / b - 1 : a / b;
		}

		/// <summary>
		/// Konvertuje JObject na 2D vektor.
		/// </summary>
		/// <returns>Barva.</returns>
		/// <param name="obj">JObject.</param>
		public static Vector2 JTokenToVector2(Newtonsoft.Json.Linq.JToken obj)
		{
			var clrs = ((string)obj).Split(';');
			return new Vector2(float.Parse(clrs[0], NumberStyles.Any, CultureInfo.InvariantCulture), float.Parse(clrs[1], NumberStyles.Any, CultureInfo.InvariantCulture));
		}

		/// <summary>
		/// Konvertuje JObject na barvu.
		/// </summary>
		/// <returns>Barva.</returns>
		/// <param name="obj">JObject.</param>
		public static Color JTokenToColor(Newtonsoft.Json.Linq.JToken obj)
		{
			var clrs = ((string)obj).Split(';');
			return new Color(byte.Parse(clrs[0]), byte.Parse(clrs[1]), byte.Parse(clrs[2]), (clrs.Length >= 4 ? byte.Parse(clrs[3]) : (byte)255));
		}

		/// <summary>
		/// Ulozi JSON serializovatlny objekt do souboru.
		/// </summary>
		/// <param name="obj">Objekt.</param>
		public static void WriteToJsonFile(IJsonSerializable obj, string filename, bool indented = true)
		{
			using (System.IO.FileStream stream = new System.IO.FileStream(filename, System.IO.FileMode.Create))
			using (System.IO.StreamWriter sWriter = new System.IO.StreamWriter(stream))
			using (Newtonsoft.Json.JsonTextWriter writer = new Newtonsoft.Json.JsonTextWriter(sWriter))
			{
				if (indented)
					writer.Formatting = Newtonsoft.Json.Formatting.Indented;
				obj.WriteToJson(writer);
			}
		}
	}
}
