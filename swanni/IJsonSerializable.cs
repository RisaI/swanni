﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
namespace swanni
{
	public interface IJsonSerializable
	{
		void WriteToJson(JsonWriter writer);
		void ReadFromJson(JObject obj);
	}
}
