﻿using System;
using System.IO;
using System.IO.Compression;
namespace swanni.Game
{
	public partial class GameWorld
	{
		/// <summary>
		/// Serializuje GameWorld pomoci BinaryWriter instance.
		/// </summary>
		/// <param name="writer">Writer.</param>
		public void Serialize(BinaryWriter writer)
		{
			writer.Write(TimeScale);

			// Globalni registr
			writer.Write(GlobalRegister);

			// Lokace
			writer.Write(CurrentLocation.AssetName);

			//Entity
			writer.Write(ActiveEntities.Count);
			_activeEntities.ForEach(ent => 
			{
				writer.Write(ent.Unique);
				if (ent.Unique)
					writer.Write(ent.UID);
				else
					ent.Serialize(writer);
			});
		}

		/// <summary>
		/// Deserializuje GameWorld.
		/// </summary>
		/// <param name="reader">Reader.</param>
		public void Deserialize(BinaryReader reader)
		{
			TimeScale = reader.ReadSingle();

			//Nacist registr
			GlobalRegister.Deserialize(reader);

			//Lokace
			CurrentLocation = ContentLoader.Locations[reader.ReadString()];

			//Entity
			_activeEntities.Clear();
			var eCount = reader.ReadInt32();
			for (int i = 0; i < eCount; ++i)
			{
				if (!reader.ReadBoolean())
				{
					var ent = new Entity();
					ent.Deserialize(reader);
					AddEntity(ent);
				}
				else
				{
					AddEntity(GlobalRegister.GetEntityFromRegister(reader.ReadGuid()));
				}
			}
		}

		/// <summary>
		/// Ulozi a zkomprimuje herni stav do souboru.
		/// </summary>
		/// <param name="filename">Nazev (cesta) souboru.</param>
		public void SaveToFile(string filename)
		{
			using (FileStream stream = new FileStream(filename, FileMode.Create))
			using (GZipStream compression = new GZipStream(stream, CompressionMode.Compress))
			using (BinaryWriter writer = new BinaryWriter(compression))
			{
				Serialize(writer);
			}
		}

		public void LoadFromFile(string filename)
		{
			using (FileStream stream = new FileStream(filename, FileMode.Open))
			using (GZipStream compression = new GZipStream(stream, CompressionMode.Decompress))
			using (BinaryReader reader = new BinaryReader(compression))
			{
				Deserialize(reader);
			}
		}
	}
}
