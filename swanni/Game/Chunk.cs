﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace swanni
{
	public class Chunk : ISerializable
	{
		//PX - v pixelech, T - v poctu tiles
		public const int ChunkSize_T = 32, TileSize_PX = 16, ChunkSize_PX = ChunkSize_T * TileSize_PX;

		/// <summary>
		/// Pole tile.
		/// </summary>
		public Tile[] Tiles;

		public Chunk()
		{
			Tiles = new Tile[ChunkSize_T * ChunkSize_T];
		}

		/// <summary>
		/// Nastavi vsem tile dane udaje.
		/// </summary>
		/// <param name="tileset">Tileset.</param>
		/// <param name="id">ID.</param>
		/// <param name="collision">Kolize.</param>
		public void Clear(byte tileset, UInt16 id, bool collision = false)
		{
			for (int i = 0; i < Tiles.Length; ++i)
				Tiles[i] = new Tile() { Tileset = tileset, ID = id, Collision = collision };
		}

		/// <summary>
		/// Serializuje chunku.
		/// </summary>
		/// <param name="writer">Writer.</param>
		public void Serialize(System.IO.BinaryWriter writer)
		{
			for (int i = 0; i < Tiles.Length; ++i)
			{
				writer.Write(Tiles[i].Tileset);
				writer.Write(Tiles[i].ID);
				writer.Write(Tiles[i].Collision);
			}
		}

		/// <summary>
		/// Deserializuje chunku.
		/// </summary>
		/// <param name="reader">Reader.</param>
		public void Deserialize(System.IO.BinaryReader reader)
		{
			for (int i = 0; i < Tiles.Length; ++i)
			{
				Tiles[i].Tileset = reader.ReadByte();
				Tiles[i].ID = reader.ReadUInt16();
				Tiles[i].Collision = reader.ReadBoolean();
			}
		}

		/// <summary>
		/// Vykresli vsechny Tile teto chunk instance na dane souradnici.
		/// </summary>
		/// <param name="spriteBatch">Sprite batch.</param>
		/// <param name="position">Pozice v PX.</param>
		/// <param name="drawDebugData">Pokud <c>true</c>, budou vykreslena debugova data (kolize...).</param>
		internal void Draw(SpriteBatch spriteBatch, Vector2 position, bool drawDebugData = false)
		{
			for (int i = 0; i < ChunkSize_T * ChunkSize_T; ++i)
			{
				var tile = Tiles[i];
				spriteBatch.Draw(ContentLoader.Tilesets[tile.Tileset], position + new Vector2(i % ChunkSize_T, i / ChunkSize_T) * TileSize_PX,
				                 ContentLoader.IdToTilesetPosition(tile.Tileset, tile.ID), drawDebugData && tile.Collision ? Color.Red : Color.White, 0, Vector2.Zero, Vector2.One, SpriteEffects.None, 0f);
			}
		}
	}
}
