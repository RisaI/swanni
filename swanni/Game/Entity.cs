﻿using System;
using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

using Newtonsoft.Json.Linq;

using swanni.Game.Components;

namespace swanni.Game
{
	[Serialization("entity")]
	public sealed class Entity : ISerializable, IJsonSerializable, ICloneable<Entity>
	{
		/// <summary>
		/// Unikatni identifikator entity. POZN.: v budoucnu mozna nahradime necim mensim, Guid je pro nase potreby moc overkill.
		/// </summary>
		public Guid UID;

		/// <summary>
		/// GameWorld do ktereho entita patri.
		/// </summary>
		/// <value>GameWorld.</value>
		public GameWorld World
		{
			get;
			private set;
		}

		/// <summary>
		/// List komponentu.
		/// </summary>
		private List<EntityComponent> Components;

		/// <summary>
		/// Pozice entity.
		/// </summary>
		public Vector2 Position;

		/// <summary>
		/// Unikatni tag entity.
		/// </summary>
		/// <value>The unique tag.</value>
		public string AssetName
		{
			get;
			internal set;
		}

		/// <summary>
		/// Je entita unikatni?
		/// </summary>
		public bool Unique
		{
			get;
			private set;
		}

		/// <summary>
		/// Hlavni konstruktor entity.
		/// </summary>
		public Entity()
		{
			UID = Guid.NewGuid();
			Components = new List<EntityComponent>();
		}

		/// <summary>
		/// Priradi neunikatni entite unikatni tag a tim padem ji zunikatni.
		/// </summary>
		/// <param name="tag">Tag, pokud null, UniqueTagem se stane UID.ToString().</param>
		internal void MakeUnique(string tag)
		{
			if (!Unique)
			{
				if (tag != null)
					AssetName = tag;
				else
					AssetName = UID.ToString();
			}
		}

		/// <summary>
		/// Metoda, ktera je volana pri zarazeni entity do seznamu aktivnich entit.
		/// </summary>
		/// <param name="world">GameWorld.</param>
		public void OnSpawn(GameWorld world)
		{
			World = world;
			Components.ForEach(c => c.OnSpawn());
		}

		/// <summary>
		/// Update metoda entity.
		/// </summary>
		/// <returns>The update.</returns>
		/// <param name="gameTime">Game time.</param>
		public void Update(GameTime gameTime)
		{
			//Updatovat vsechny komponenty
			Components.ForEach(c => c.Update(gameTime));
		}

		/// <summary>
		/// Vykresli vechny komponenty entity.
		/// </summary>
		/// <param name="spriteBatch">SpriteBatch.</param>
		public void Draw(SpriteBatch spriteBatch)
		{
			//Vykreslit vsechny komponenty
			Components.ForEach(c => c.Draw(spriteBatch));
		}

		/// <summary>
		/// Prida komponent pod entitu.
		/// </summary>
		/// <returns>Vrati odkaz na tuto instanci Entity pro retezeni.</returns>
		/// <param name="component">Komponent.</param>
		public Entity AddComponent(EntityComponent component)
		{
			if (!Components.Contains(component))
			{
				Components.Add(component);
				component.Attach(this);
			}

			return this;
		}

		/// <summary>
		/// Prida vice komponentu pod entitu.
		/// </summary>
		/// <returns>Vrati odkaz na tuto instanci Entity pro retezeni.</returns>
		/// <param name="components">Komponenty.</param>
		public Entity AddComponents(params EntityComponent[] components)
		{
			if (components != null)
				for (int i = 0; i < components.Length; ++i)
				{
					AddComponent(components[i]);
				}

			return this;
		}

		/// <summary>
		/// Vrati instanci komponentu typu T.
		/// </summary>
		/// <returns>Komponent.</returns>
		/// <typeparam name="T">Podtrida EntityComponent.</typeparam>
		public T GetComponent<T>() where T : EntityComponent
		{
			return Components.Find(c => c is T) as T;
		}

		/// <summary>
		/// Serializuje entitu.
		/// </summary>
		/// <param name="writer">Writer.</param>
		public void Serialize(System.IO.BinaryWriter writer)
		{
			writer.Write(UID);
			writer.Write(Position);
			writer.Write(AssetName);
			writer.Write(Unique);
			writer.Write((byte)Components.Count);
			for (int i = 0; i < Components.Count; ++i)
			{
				writer.Write(Components[i]);
			}
		}

		/// <summary>
		/// Deserializuje entitu.
		/// </summary>
		/// <param name="reader">Reader.</param>
		public void Deserialize(System.IO.BinaryReader reader)
		{
			UID = reader.ReadGuid();
			Position = reader.ReadVector2();
			AssetName = reader.ReadString();
			Unique = reader.ReadBoolean();
			byte c = reader.ReadByte();
			for (int i = 0; i < c; ++i)
			{
				var component = EntityComponent.CreateInstanceDeserialize(reader);
				AddComponent(component);
			}
		}

		public void ReadFromJson(JObject obj)
		{
			if (obj["unique"] != null)
				Unique = (bool)obj["unique"];
			var components = (JArray)obj.GetValue("components");
			for (int i = 0; i < components.Count; ++i)
			{
				var component = EntityComponent.CreateInstanceDeserialize((JObject)components[i]);
				AddComponent(component);
			}
		}

		public void WriteToJson(Newtonsoft.Json.JsonWriter writer)
		{
			writer.WriteStartObject();
			if (Unique)
			{
				writer.WritePropertyName("unique");
				writer.WriteValue(Unique);
			}
			writer.WritePropertyName("components");
			writer.WriteStartArray(); //komponenty
			Components.ForEach(c => c.WriteToJson(writer));
			writer.WriteEndArray();
			writer.WriteEndObject();
		}

		/// <summary>
		/// Vytvori klon teto instance entity.
		/// </summary>
		/// <returns>Klon.</returns>
		public Entity Clone()
		{
			var ent = new Entity() { Position = Position, AssetName = AssetName, Unique = Unique };
			Components.ForEach(c => ent.AddComponent(c.Clone()));
			return ent;
		}
	}
}