﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
namespace swanni.Game.Components
{
	[Serialization("bodycomponent")]
	public class BodyComponent : EntityComponent
	{
		public Vector2 Velocity, Size, Anchor = new Vector2(0.5f);

		public bool OnGround
		{
			get;
			set;
		}

		public BodyComponent()
		{

		}

		public BodyComponent(Vector2 size, Vector2 anchor)
		{
			Size = size;
			Anchor = anchor;
		}

		protected override void OnAttach()
		{
			return;
		}

		public override void OnSpawn()
		{
			return;
		}

		public override void Update(GameTime gameTime)
		{
			if (Math.Abs(Velocity.X) > 0.01f)
			{
				var step = Velocity.X * (float)gameTime.ElapsedGameTime.TotalSeconds * World.TimeScale;
				var dst = World.GetHorizontalDistanceFromCollision(Entity.Position + Size * (new Vector2(0.5f) - Anchor), Size, step);
				if (Math.Abs(dst) <= Math.Abs(step))
				{
					Entity.Position.X += dst;
					Velocity.X = 0;
				}
				else
				{
					Entity.Position.X += step;
				}
			}

			if (Velocity.Y > 0)
			{
				if (!OnGround)
				{
					var step = Velocity.Y * (float)gameTime.ElapsedGameTime.TotalSeconds * World.TimeScale;
					var dst = World.GetVerticalDistanceFromCollision(Entity.Position + Size * (new Vector2(0.5f) - Anchor), Size, step);
					if (Math.Abs(dst) <= Math.Abs(step))
					{
						Entity.Position.Y += dst;
						Velocity.Y = 0;
						OnGround = true;
					}
					else
					{
						Entity.Position.Y += step;
					}
				}
				else
				{
					Velocity.Y = 0;
				}
			}
			else if (Velocity.Y < 0)
			{
				OnGround = false;
				Entity.Position.Y += Velocity.Y * (float)gameTime.ElapsedGameTime.TotalSeconds * World.TimeScale;
			}
			else if (World.GetVerticalDistanceFromCollision(Entity.Position + Size * (new Vector2(0.5f) - Anchor), Size, 1) >= 1)
			{
				OnGround = false;
			}

			if (!OnGround)
				Velocity.Y += World.Gravity * (float)gameTime.ElapsedGameTime.TotalSeconds * World.TimeScale;
		}
	
		public override void Draw(SpriteBatch spriteBatch)
		{
			return;
		}

		protected override void OnSerialize(System.IO.BinaryWriter writer)
		{
			writer.Write(Velocity);
			writer.Write(Size);
			writer.Write(Anchor);
			writer.Write(OnGround);
		}

		protected override void OnDeserialize(System.IO.BinaryReader reader)
		{
			Velocity = reader.ReadVector2();
			Size = reader.ReadVector2();
			Anchor = reader.ReadVector2();
			OnGround = reader.ReadBoolean();
		}

		protected override void OnJsonRead(Newtonsoft.Json.Linq.JObject obj)
		{
			Size = Helper.JTokenToVector2(obj["size"]);
			if (obj["anchor"] != null)
				Anchor = Helper.JTokenToVector2(obj["anchor"]);
		}

		protected override void OnJsonWrite(Newtonsoft.Json.JsonWriter writer)
		{
			writer.WritePropertyName("size");
			writer.WriteValue(Size);
			writer.WritePropertyName("anchor");
			writer.WriteValue(Anchor);
		}

		public override EntityComponent Clone()
		{
			return new BodyComponent(Size, Anchor) { Velocity = Velocity };
		}
	}
}
