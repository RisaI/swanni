﻿using System;
namespace swanni.Game.Components
{
	[Serialization("spritecomponent")]
	public class SpriteComponent : EntityComponent
	{
		private string _spriteAsset;
		private Sprite _sprite;
		public Sprite Sprite
		{
			get { return _sprite; }
		}

		public float Depth;

		public SpriteComponent() : base()
		{

		}

		public SpriteComponent(string spriteAsset)
		{
			_spriteAsset = spriteAsset;
			_sprite = ContentLoader.GetSprite(spriteAsset);
		}

		protected override void OnAttach()
		{
			return;
		}

		public override void OnSpawn()
		{
			return;
		}

		public override void Update(Microsoft.Xna.Framework.GameTime gameTime)
		{
			if (_sprite != null)
				_sprite.Update(gameTime, Entity.World.TimeScale);
		}

		public override void Draw(Microsoft.Xna.Framework.Graphics.SpriteBatch spriteBatch)
		{
			if (_sprite != null)
				_sprite.Draw(spriteBatch, Entity.Position, World.ActiveCamera.GetDepth(Entity.Position));
		}

		protected override void OnDeserialize(System.IO.BinaryReader reader)
		{
			_sprite = ContentLoader.GetSprite(_spriteAsset = reader.ReadString());
			_sprite.DeserializeState(reader);
		}

		protected override void OnSerialize(System.IO.BinaryWriter writer)
		{
			writer.Write(_spriteAsset);
			Sprite.SerializeState(writer);
		}

		protected override void OnJsonWrite(Newtonsoft.Json.JsonWriter writer)
		{
			writer.WritePropertyName("sprite");
			writer.WriteValue(_spriteAsset);
		}

		protected override void OnJsonRead(Newtonsoft.Json.Linq.JObject obj)
		{
			_spriteAsset = (string)obj["sprite"];
			_sprite = ContentLoader.GetSprite(_spriteAsset);
		}

		public override EntityComponent Clone()
		{
			return new SpriteComponent(_spriteAsset);
		}
	}
}
