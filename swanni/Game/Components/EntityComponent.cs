﻿using System;
using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

namespace swanni.Game.Components
{
	public abstract class EntityComponent : ISerializable, ICloneable<EntityComponent>, IJsonSerializable
	{
		/// <summary>
		/// Vlastnici entita.
		/// </summary>
		/// <value>Entita.</value>
		public Entity Entity
		{
			get;
			private set;
		}

		/// <summary>
		/// Vraci svet, ve kterem entita zije. Jde jen o zkratku.
		/// </summary>
		/// <value>GameWorld.</value>
		public GameWorld World
		{
			get { return Entity.World; }
		}

		public EntityComponent()
		{
			
		}

		/// <summary>
		/// Pripne kompenent k entite.
		/// </summary>
		/// <param name="ent">Entita.</param>
		public void Attach(Entity ent)
		{
			Entity = ent;
			OnAttach();
		}

		/// <summary>
		/// Metoda, ktera je volana pri "pripnuti" komponentu k entite. GameWorld bude s nejvetsi pravdepodobnosti null.
		/// </summary>
		protected abstract void OnAttach();

		/// <summary>
		/// Metoda, ktera je volana pri pridani entity do kolekce aktivnich entit nektere instance GameWorld. Reference na instanci GameWorld je zajistena.
		/// </summary>
		public abstract void OnSpawn();

		/// <summary>
		/// Update metoda, volana pri kazdem Update callu aktivnich entit. Reference na instanci GameWorld je zajistena.
		/// </summary>
		/// <param name="gameTime">Game time.</param>
		public abstract void Update(GameTime gameTime);

		/// <summary>
		/// Draw metoda, volana pri kazdem Draw callu aktivnich entit. Reference na instanci GameWorld je zajistena.
		/// </summary>
		/// <param name="spriteBatch">Sprite batch.</param>
		public abstract void Draw(SpriteBatch spriteBatch);

		/// <summary>
		/// Serializuje komponent.
		/// </summary>
		/// <param name="writer">Writer.</param>
		public void Serialize(System.IO.BinaryWriter writer)
		{
			var attributes = GetType().GetCustomAttributes(typeof(SerializationAttribute), false);
			if (attributes.Length == 0)
				return;
			writer.Write((attributes[0] as SerializationAttribute).Identifier);
			OnSerialize(writer);
		}

		/// <summary>
		/// Deserializuje komponent.
		/// </summary>
		/// <param name="reader">Reader.</param>
		public void Deserialize(System.IO.BinaryReader reader)
		{
			OnDeserialize(reader);
		}

		/// <summary>
		/// Vlastni serializace.
		/// </summary>
		/// <param name="writer">Writer.</param>
		protected abstract void OnSerialize(System.IO.BinaryWriter writer);

		/// <summary>
		/// Vlastni deserializace.
		/// </summary>
		/// <param name="reader">Reader.</param>
		protected abstract void OnDeserialize(System.IO.BinaryReader reader);

		/// <summary>
		/// Serializuje objekt pomoci JsonTextWriteru
		/// </summary>
		/// <param name="writer">Writer.</param>
		public void WriteToJson(Newtonsoft.Json.JsonWriter writer)
		{
			var attributes = GetType().GetCustomAttributes(typeof(SerializationAttribute), false);
			if (attributes.Length == 0)
				return;
			
			writer.WriteStartObject();
			writer.WritePropertyName("name");
			writer.WriteValue(((SerializationAttribute)attributes[0]).Identifier);
			OnJsonWrite(writer);
			writer.WriteEndObject();
		}

		/// <summary>
		/// Deserializuje komponent z JSON objektu
		/// </summary>
		/// <param name="obj">Object.</param>
		public void ReadFromJson(Newtonsoft.Json.Linq.JObject obj)
		{
			OnJsonRead(obj);
		}

		/// <summary>
		/// Vlastni JSON serializace.
		/// </summary>
		/// <param name="writer">Writer.</param>
		protected abstract void OnJsonWrite(Newtonsoft.Json.JsonWriter writer);

		/// <summary>
		/// Vlastni JSON deserializace.
		/// </summary>
		/// <param name="obj">Object.</param>
		protected abstract void OnJsonRead(Newtonsoft.Json.Linq.JObject obj);

		/// <summary>
		/// Vrati klon komponentu nalezici dane entite.
		/// </summary>
		/// <returns>Klon.</returns>
		public abstract EntityComponent Clone();

		/// <summary>
		/// Vytvori instanci komponentu a deserializuje jej z binary readeru.
		/// </summary>
		/// <returns>Nova instance EntityComponent podtridy.</returns>
		/// <param name="reader">Reader.</param>
		public static EntityComponent CreateInstanceDeserialize(System.IO.BinaryReader reader)
		{
			EntityComponent component = (EntityComponent)DeserializationRegister.GetInstance(reader.ReadString());
			component.Deserialize(reader);
			return component;
		}

		public static EntityComponent CreateInstanceDeserialize(Newtonsoft.Json.Linq.JObject obj)
		{
			EntityComponent component = (EntityComponent)DeserializationRegister.GetInstance((string)obj["name"]);
			component.ReadFromJson(obj);
			return component;
		}
	}
}