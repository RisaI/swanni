﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace swanni.Game.Components
{
	[Serialization("playerinputcomponent")]
	public class PlayerInputComponent : EntityComponent
	{
		public PlayerInputComponent()
		{
		}

		protected override void OnAttach()
		{
			bodyComponent = Entity.GetComponent<BodyComponent>();
			spriteComponent = Entity.GetComponent<SpriteComponent>();
		}

		public override void OnSpawn()
		{
			return;
		}

		private BodyComponent bodyComponent;
		public override void Update(Microsoft.Xna.Framework.GameTime gameTime)
		{
			if (bodyComponent == null)
			{
				bodyComponent = Entity.GetComponent<BodyComponent>();
				if (bodyComponent == null)
					return;
			}

			bool xMov = false;

			/*if (Input.Binds["down"].IsDown())
				vel.Y += Chunk.TileSize_PX * 8;*/

			if (Input.Binds["up"].IsPressed())
			{
				if (bodyComponent.OnGround)
				{
					bodyComponent.Velocity.Y -= 280;
					bodyComponent.OnGround = false;
					PlayAnim("jump");
				}
			}

			if (Input.Binds["left"].IsDown())
			{
				if (bodyComponent.Velocity.X > -120)
					bodyComponent.Velocity.X -= 20 * World.TimeScale;
				SetEffect(SpriteEffects.FlipHorizontally);
				if (bodyComponent.OnGround)
					PlayAnim("walk");
                xMov = true;
			}
			if (Input.Binds["right"].IsDown())
			{
				if (bodyComponent.Velocity.X < 120)
					bodyComponent.Velocity.X += 20 * World.TimeScale;
				SetEffect(SpriteEffects.None);
				if (bodyComponent.OnGround)
					PlayAnim("walk");
                xMov = true;
			}


			if (!xMov)
			{
				if (bodyComponent.OnGround)
				{
					bodyComponent.Velocity.X /= (1 + .15f * World.TimeScale);
					PlayAnim("idle");
				}
				else
				{
					PlayAnim("jump");
				}
			}


			if (World.ActiveCamera != null)
				World.ActiveCamera.Position = Entity.Position;
		}

		private SpriteComponent spriteComponent;
		private void PlayAnim(string animation)
		{
			if (spriteComponent == null)
			{
				spriteComponent = Entity.GetComponent<SpriteComponent>();
				if (spriteComponent == null)
					return;
			}
			spriteComponent.Sprite.PlayAnimationFromRegistry(animation, false);
		}

		private void StopAnim()
		{
			if (spriteComponent == null)
			{
				spriteComponent = Entity.GetComponent<SpriteComponent>();
				if (spriteComponent == null)
					return;
			}
			spriteComponent.Sprite.StopAnimation();
		}

		private void SetEffect(SpriteEffects effect)
		{
			if (spriteComponent == null)
			{
				spriteComponent = Entity.GetComponent<SpriteComponent>();
				if (spriteComponent == null)
					return;
			}
			spriteComponent.Sprite.Effect = effect;
		}

		public override void Draw(Microsoft.Xna.Framework.Graphics.SpriteBatch spriteBatch)
		{
			return;
		}

		protected override void OnSerialize(System.IO.BinaryWriter writer)
		{
			return;
		}

		protected override void OnDeserialize(System.IO.BinaryReader reader)
		{
			return;
		}

		protected override void OnJsonRead(Newtonsoft.Json.Linq.JObject obj)
		{
			return;
		}

		protected override void OnJsonWrite(Newtonsoft.Json.JsonWriter writer)
		{
			return;
		}

		public override EntityComponent Clone()
		{
			return new PlayerInputComponent();
		}
	}
}
