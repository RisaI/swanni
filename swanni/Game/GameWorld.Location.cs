﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using Microsoft.Xna.Framework;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
namespace swanni.Game
{
	public partial class GameWorld
	{
		/// <summary>
		/// Soucasne nactena lokace.
		/// </summary>
		/// <value>Soucasna lokace.</value>
		public Location CurrentLocation
		{
			get;
			private set;
		}

		/// <summary>
		/// Nacte lokaci.
		/// </summary>
		/// <param name="loc">Lokace.</param>
		public void LoadLocation(Location loc)
		{
			CurrentLocation = loc;
			_activeEntities.Clear();
			for (int i = GlobalRegister.Travellers.Count - 1; i >= 0; --i)
			{
				var traveller = GlobalRegister.Travellers[i];
				if (traveller.Location == loc.AssetName && loc.TravelNodes.ContainsKey(traveller.TravelNode))
				{
					SpawnEntity(traveller.UniqueTag, loc.TravelNodes[traveller.TravelNode]);
					GlobalRegister.Travellers.RemoveAt(i);
				}
			}
			// TODO: nacitaci veci
		}

		internal void CreateLocation(string name, int width, int height)
		{
			CurrentLocation = new Location(name, width, height);
		}

		/// <summary>
		/// Nacte lokaci.
		/// </summary>
		/// <param name="assetName">Asset nazev lokace.</param>
		public void LoadLocation(string assetName)
		{
			LoadLocation(ContentLoader.Locations[assetName]);
		}

		public void Travel(string uniqueTag, string travelLocation, string travelNode)
		{
			if (GlobalRegister.ContainsEntity(uniqueTag))
			{
				if (CurrentLocation != null && CurrentLocation.AssetName == travelLocation)
				{
					SpawnEntity(uniqueTag, CurrentLocation.TravelNodes[travelNode]);
				}
				else
				{
					var ent = GlobalRegister.GetEntityFromRegister(uniqueTag);
					RemoveActiveEntity(ent);
					GlobalRegister.Travellers.Add(new WorldRegister.TravelInfo(ent, travelLocation, travelNode));
				}
			}
		}

		public class Location : IJsonSerializable
		{
			/// <summary>
			/// Asset jmeno lokace.
			/// </summary>
			/// <value>Asset jmeno.</value>
			public string AssetName
			{
				get;
				private set;
			}

			/// <summary>
			/// Sirka mapy v chuncich.
			/// </summary>
			/// <value>Sirka mapy v chuncich.</value>
			public int MapWidth_CH
			{
				get;
				private set;
			}

			/// <summary>
			/// Vyska mapy v chuncich.
			/// </summary>
			/// <value>Vyska mapy v chuncich.</value>
			public int MapHeight_CH
			{
				get { return Chunks != null && MapWidth_CH > 0 ? Chunks.Length / MapWidth_CH : 0; }
			}

			/// <summary>
			/// Pole chunku
			/// </summary>
			/// <value>Chunky.</value>
			public Chunk[] Chunks
			{
				get;
				private set;
			}

			public Dictionary<string, Vector2> TravelNodes
			{
				get;
				private set;
			}

			/// <summary>
			/// Nacte lokaci ze souboru, pokusi se najit tilemapu.
			/// </summary>
			/// <returns>Nactena lokace.</returns>
			/// <param name="name">Nazev.</param>
			/// <param name="filename">Soubor.</param>
			public static Location LoadFromFile(string name, string filename)
			{
				var loc = new Location() { AssetName = name };

				using (FileStream stream = new FileStream(filename, FileMode.Open))
				using (StreamReader sReader = new StreamReader(stream))
				using (JsonTextReader reader = new JsonTextReader(sReader))
				{
					loc.ReadFromJson((JObject)JObject.ReadFrom(reader));
				}

				var tilemapFilename = filename.Replace(Path.GetExtension(filename), ".tilemap");
				if (File.Exists(tilemapFilename))
				{
					using (FileStream stream = new FileStream(tilemapFilename, FileMode.Open))
					using (GZipStream compression = new GZipStream(stream, CompressionMode.Decompress))
					using (BinaryReader reader = new BinaryReader(compression))
					{
						loc.LoadTileMap(reader);
					}
				}
				else
				{
					loc.Chunks = new Chunk[0];
				}

				return loc;
			}					

			protected Location()
			{
				TravelNodes = new Dictionary<string, Vector2>();
			}

			public Location(string name, int mapwidth, int mapheight) : this()
			{
				AssetName = name;
				MapWidth_CH = mapwidth;
				Chunks = new Chunk[mapwidth * mapheight];
				for (int i = 0; i < Chunks.Length; ++i)
				{
					Chunks[i] = new Chunk();
				}
			}

			/// <summary>
			/// Zapise JSON informace do writeru.
			/// </summary>
			/// <param name="writer">Writer.</param>
			public void WriteToJson(JsonWriter writer)
			{
				writer.WriteStartObject();
				writer.WritePropertyName("nodes");
				writer.WriteStartObject();
				TravelNodes.ToList().ForEach(pair =>
				{
					writer.WritePropertyName(pair.Key);
					writer.WriteVector2(pair.Value);
				});
				writer.WriteEndObject();
				writer.WriteEndObject();
			}

			/// <summary>
			/// Precte JSON udaje.
			/// </summary>
			/// <param name="obj">JObject.</param>
			public void ReadFromJson(JObject obj)
			{
				if (obj["nodes"] != null)
				{
					var nodes = (JObject)obj["nodes"];
					TravelNodes.Clear();
					foreach (JToken token in nodes.Children())
					{
						if (token.Type == JTokenType.Property)
							TravelNodes.Add((token as JProperty).Name, Helper.JTokenToVector2((token as JProperty).Value));
					}
				}
			}

			/// <summary>
			/// Ulozi tilemapu
			/// </summary>
			/// <param name="writer">Writer.</param>
			public void SaveTileMap(BinaryWriter writer)
			{
				writer.Write(MapWidth_CH);
				writer.Write(Chunks.Length);
				for (int i = 0; i < Chunks.Length; ++i)
				{
					Chunks[i].Serialize(writer);
				}
			}

			/// <summary>
			/// Nacte tilemapu
			/// </summary>
			/// <param name="reader">Reader.</param>
			public void LoadTileMap(BinaryReader reader)
			{
				MapWidth_CH = reader.ReadInt32();
				Chunks = new Chunk[reader.ReadInt32()];
				for (int i = 0; i < Chunks.Length; ++i)
				{
					Chunks[i] = new Chunk();
					Chunks[i].Deserialize(reader);
				}
			}

			/// <summary>
			/// Ulozi JSON udaje a tilemapu
			/// </summary>
			/// <param name="filename">Nazev souboru.</param>
			public void SaveToFile(string filename)
			{
				// Informace o lokaci
				Helper.WriteToJsonFile(this, filename + ".location");

				// Tile mapa
				using (FileStream stream = new FileStream(filename + ".tilemap", FileMode.Create))
				using (GZipStream compression = new GZipStream(stream, CompressionMode.Compress))
				using (BinaryWriter writer = new BinaryWriter(compression))
				{
					SaveTileMap(writer);
				}
			}

			/// <summary>
			/// Zmeni velikost tilemapy a zkopiruje prekryvajici se chunky z puvodni mapy.
			/// </summary>
			/// <param name="width">Sirka v CH.</param>
			/// <param name="height">Vyska v CH.</param>
			internal void ResizeChunkMap(int width, int height)
			{
				if (Chunks == null)
				{
					Chunks = new Chunk[width * height];
					MapWidth_CH = width;
				}
				else
				{
					var newChunks = new Chunk[width * height];
					for (int x = 0; x < width; ++x)
					{
						for (int y = 0; y < height; ++y)
						{
							if (x < MapWidth_CH && y < MapHeight_CH)
								newChunks[x + y * width] = Chunks[x + y * MapWidth_CH];
							else
								newChunks[x + y * width] = new Chunk();
						}
					}
					Chunks = newChunks;
					MapWidth_CH = width;
				}
			}
		}
	}
}
