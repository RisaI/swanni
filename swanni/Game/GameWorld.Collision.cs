﻿using System;
using Microsoft.Xna.Framework;

namespace swanni.Game
{
	public partial class GameWorld //Kolize
	{
		/// <summary>
		/// Zjisti, jestli se v dane horizontalni vzdalenosti nachazi tile a vrati skutecnou vzdalenost.
		/// </summary>
		/// <returns>Horizontalni vzdalenost pozice od nejblizsi hrany kolizni tile v range</returns>
		/// <param name="position">Pozice v PX jednotkach.</param>
		/// <param name="range">Rozpeti v PX jednotkach. Zaporne hodnoty jdou doleva, kladne doprava.</param>
		public float GetHorizontalDistanceFromCollision(Vector2 position, float range)
		{
			int startX = range > 0 ? 1 + Helper.NegDivision((int)(position.X - 1), Chunk.TileSize_PX) :  //Pokud je smer kladny (doprava), zacit o 1 tile vpravo od pozice (nekontrolovat tile "v" pozici)
						  Helper.NegDivision((int)(position.X + 1), Chunk.TileSize_PX) - 1, //Pokud je smer zaporny (doleva), zacit o 1 tile vlevo od pozice
				startY = Helper.NegDivision((int)(position.Y + 1), Chunk.TileSize_PX); //Prevest Y koordinat z PX na T jednotky

			int rayWidth = 1 + Math.Abs((int)range) / Chunk.TileSize_PX; //Delka "paprsku" (kolik tile se ma zkontrolovat)

			for (int x = 0; x < rayWidth; ++x) //Projde vsechny tile v draze paprsku
			{
				if (GetTileOnPosition_T(startX + (range > 0 ? x : -x), startY).Collision) //Pokud je tile "kolizni"
				{
					return range > 0 ? (startX + x) * Chunk.TileSize_PX - position.X
						: (startX - x + 1) * Chunk.TileSize_PX - position.X - 1; //Vrati horizontalni vzdalenost pozice od nejblizsi hrany tile v PX jednotkach
				}
			}
			return float.MaxValue; //Vratit "nekonecno", pokud v draze neni zadna kolizni tile
		}

		/// <summary>
		/// Zjisti, jestli se v dane horizontalni vzdalenosti od stredu obdelniku nachazi tile a vrati skutecnou vzdalenost steny tile a steny obdelnika.
		/// </summary>
		/// <returns>Horizontalni vzdalenost hrany obdelnika od nejblizsi hrany kolizni tile v range</returns>
		/// <param name="position">Pozice v PX jednotkach.</param>
		/// <param name="size">Rozmery obdelnika v PX jednotkach.</param>
		/// <param name="range">Rozpeti v PX jednotkach. Zaporne hodnoty jdou doleva, kladne doprava.</param>
		public float GetHorizontalDistanceFromCollision(Vector2 position, Vector2 size, float range)
		{
			int startX = range > 0 ? 1 + Helper.NegDivision((int)(position.X - 1 + size.X / 2), Chunk.TileSize_PX) : Helper.NegDivision((int)(position.X + 1 - size.X / 2), Chunk.TileSize_PX) - 1,
				startY = Helper.NegDivision((int)(position.Y + 1 - size.Y / 2), Chunk.TileSize_PX);
			int rayHeight = 1 + (Helper.NegDivision((int)(position.Y - 1 + size.Y / 2), Chunk.TileSize_PX) - startY), rayWidth = 1 + Math.Abs((int)range) / Chunk.TileSize_PX;
			for (int x = 0; x < rayWidth; ++x)
			{
				for (int y = 0; y < rayHeight; ++y)
				{
					if (GetTileOnPosition_T(startX + (range > 0 ? x : -x), startY + y).Collision)
					{
						return range > 0 ? (startX + x) * Chunk.TileSize_PX - position.X - size.X / 2
							: (startX - x + 1) * Chunk.TileSize_PX - position.X + size.X / 2 - 1;
					}
				}
			}
			return float.MaxValue;
		}

		/// <summary>
		/// Zjisti, jestli se v dane vertikalni vzdalenosti nachazi tile a vrati skutecnou vzdalenost.
		/// </summary>
		/// <returns>Horizontalni vzdalenost pozice od nejblizsi hrany kolizni tile v range</returns>
		/// <param name="position">Pozice v PX jednotkach.</param>
		/// <param name="range">Rozpeti v PX jednotkach. Zaporne hodnoty jdou nahoru, kladne dolu.</param>
		public float GetVerticalDistanceFromCollision(Vector2 position, float range)
		{
			int startY = range > 0 ? 1 + Helper.NegDivision((int)(position.Y - 1), Chunk.TileSize_PX) :  //Pokud je smer kladny (doprava), zacit o 1 tile vpravo od pozice (nekontrolovat tile "v" pozici)
						  Helper.NegDivision((int)(position.Y + 1), Chunk.TileSize_PX) - 1, //Pokud je smer zaporny (doleva), zacit o 1 tile vlevo od pozice
				startX = Helper.NegDivision((int)(position.X), Chunk.TileSize_PX); //Prevest Y koordinat z PX na T jednotky

			int rayHeight = 1 + Math.Abs((int)range) / Chunk.TileSize_PX; //Delka "paprsku" (kolik tile se ma zkontrolovat)

			for (int y = 0; y < rayHeight; ++y) //Projde vsechny tile v draze paprsku
			{
				if (GetTileOnPosition_T(startX, startY + (range > 0 ? y : -y)).Collision) //Pokud je tile "kolizni"
				{
					return range > 0 ? (startY + y) * Chunk.TileSize_PX - position.Y
						: (startY - y + 1) * Chunk.TileSize_PX - position.Y - 1; //Vrati horizontalni vzdalenost pozice od nejblizsi hrany tile v PX jednotkach
				}
			}
			return float.MaxValue; //Vratit "nekonecno", pokud v draze neni zadna kolizni tile
		}

		/// <summary>
		/// Zjisti, jestli se v dane vertikalni vzdalenosti od stredu obdelniku nachazi tile a vrati skutecnou vzdalenost steny tile a steny obdelnika.
		/// </summary>
		/// <returns>Horizontalni vzdalenost hrany obdelnika od nejblizsi hrany kolizni tile v range</returns>
		/// <param name="position">Pozice v PX jednotkach.</param>
		/// <param name="size">Rozmery obdelnika v PX jednotkach.</param>
		/// <param name="range">Rozpeti v PX jednotkach. Zaporne hodnoty jdou nahoru, kladne dolu.</param>
		public float GetVerticalDistanceFromCollision(Vector2 position, Vector2 size, float range)
		{
			int startY = range > 0 ? 1 + Helper.NegDivision((int)(position.Y - 1 + size.Y / 2), Chunk.TileSize_PX) :
											   Helper.NegDivision((int)(position.Y + 1 - size.Y / 2), Chunk.TileSize_PX) - 1,
				startX = Helper.NegDivision((int)(position.X + 1 - size.X / 2), Chunk.TileSize_PX);
			int rayWidth = 1 + (Helper.NegDivision((int)(position.X - 1 + size.X / 2), Chunk.TileSize_PX) - startX),
				rayHeight = 1 + Math.Abs((int)range) / Chunk.TileSize_PX;

			for (int y = 0; y < rayHeight; ++y)
			{
				for (int x = 0; x < rayWidth; ++x)
				{
					if (GetTileOnPosition_T(startX + x, startY + (range > 0 ? y : -y)).Collision)
					{
						return range > 0 ? (startY + y) * Chunk.TileSize_PX - position.Y - size.Y / 2
							: (startY - y + 1) * Chunk.TileSize_PX - position.Y + size.Y / 2 - 1;
					}
				}
			}
			return float.MaxValue;
		}

		//Rezervovano pro pripad, ze by mi jednou jeblo
		/*public float GetCollidingTileSidePosition(int startX, int startY, int rayWidth, int rayHeight, bool vertical)
		{
			if (vertical)
			{
				for (int y = Math.Sign(rayHeight); y != rayHeight; y += Math.Sign(rayHeight))
				{
					for (int x = 0; x != rayWidth; x += Math.Sign(rayWidth))
					{
						if (GetTileOnPosition_T(startX + x, startY + y).Collision)
							return (startY + y) * Chunk.TileSize_PX + (rayHeight < 0 ? Chunk.TileSize_PX - 1 : 0);
					}
				}
			}
			else
			{
				for (int x = Math.Sign(rayWidth); x != rayWidth; x += Math.Sign(rayWidth))
				{
					for (int y = 0; y != rayHeight; y += Math.Sign(rayHeight))
					{
						if (GetTileOnPosition_T(startX + x, startY + y).Collision)
							return (startX + x) * Chunk.TileSize_PX + (rayWidth < 0 ? Chunk.TileSize_PX - 1 : 0);
					}
				}
			}

			return float.MaxValue;
		}*/
	}
}
