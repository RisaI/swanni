﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

namespace swanni.Game
{
	/// <summary>
	/// GameWorld trida, drzi informace o hernim svete.
	/// </summary>
	public partial class GameWorld : ISerializable
	{
		/// <summary>
		/// Reference na Game1 objekt, ktery herni svet vlastni.
		/// </summary>
		/// <value>Game1 objekt.</value>
		public Game1 Game
		{
			get;
			private set;
		}

		/// <summary>
		/// Casova skala.
		/// </summary>
		public float TimeScale = 1f;
		public float Gravity = 38 * Chunk.TileSize_PX;

		public Camera ActiveCamera;

		/// <summary>
		/// Hlavni konstruktor GameWorld tridy.
		/// </summary>
		/// <param name="game">Game1, ktera vlastni herni svet.</param>
		public GameWorld(Game1 game)
		{
			Game = game;
			_activeEntities = new List<Entity>(256);
			ActiveEntities = _activeEntities.AsReadOnly();
			GlobalRegister = new WorldRegister();
			ActiveCamera = new Camera(game.GraphicsDevice);
		}

		/// <summary>
		/// Update mapu (jeden herni krok).
		/// </summary>
		/// <param name="gameTime">Game time.</param>
		public void Update(GameTime gameTime)
		{
			//Updatnout vsechny entity
			_activeEntities.ForEach(e => e.Update(gameTime));
			_pendingRemove.ForEach(p =>
			{
				for (int i = _activeEntities.Count - 1; i >= 0; --i)
				{
					if (_activeEntities[i].UID == p)
						_activeEntities.RemoveAt(i);
				}
			});
			_pendingRemove.Clear();
		}

		/// <summary>
		/// Vykresli herni svet v soucasnem stavu.
		/// </summary>
		/// <returns>The draw.</returns>
		/// <param name="spriteBatch">Sprite batch.</param>
		internal void Draw(SpriteBatch spriteBatch, byte debugData = 0)
		{
			//Vykreslit tilemapu
			if (CurrentLocation != null)
			{
				spriteBatch.Begin(SpriteSortMode.Texture, BlendState.AlphaBlend, SamplerState.PointClamp, null, null, null, ActiveCamera.GetMatrix());

				int startY = MathHelper.Clamp(Helper.NegDivision((int)ActiveCamera.Top, Chunk.ChunkSize_PX), 0, CurrentLocation.MapHeight_CH),
				endY = MathHelper.Clamp(Helper.NegDivision((int)ActiveCamera.Bottom, Chunk.ChunkSize_PX) + 1, 0, CurrentLocation.MapHeight_CH);

				for (int x = MathHelper.Clamp(Helper.NegDivision((int)ActiveCamera.Left, Chunk.ChunkSize_PX), 0, CurrentLocation.MapWidth_CH);
				     x < MathHelper.Clamp(Helper.NegDivision((int)ActiveCamera.Right, Chunk.ChunkSize_PX) + 1, 0, CurrentLocation.MapWidth_CH); ++x)
				{
					for (int y = startY;y < endY; ++y)
						CurrentLocation.Chunks[y * CurrentLocation.MapWidth_CH + x].Draw(spriteBatch, Chunk.ChunkSize_PX * new Vector2(x, y), (debugData & 1) == 1);
				}
				spriteBatch.End();
				if ((debugData & 2) == 2)
				{
					spriteBatch.Begin(SpriteSortMode.Texture, BlendState.AlphaBlend, SamplerState.PointClamp, null, null, null, ActiveCamera.GetMatrix());
					for (int i = 0; i < Math.Max(CurrentLocation.MapWidth_CH, CurrentLocation.MapHeight_CH) + 1; ++i)
					{
						if (i < CurrentLocation.MapWidth_CH + 1)
							spriteBatch.Draw(ContentLoader.Pixel, new Vector2(Chunk.ChunkSize_PX * i, 0), null, Color.White,
							                 0, Vector2.Zero, new Vector2(1, CurrentLocation.MapHeight_CH * Chunk.ChunkSize_PX), SpriteEffects.None, 0f);
						if (i < CurrentLocation.MapHeight_CH + 1)
							spriteBatch.Draw(ContentLoader.Pixel, new Vector2(0, Chunk.ChunkSize_PX * i), null, Color.White,
							                 0, Vector2.Zero, new Vector2(CurrentLocation.MapWidth_CH * Chunk.ChunkSize_PX, 1), SpriteEffects.None, 0f);
					}
					spriteBatch.End();
				}
			}

			//Vykreslit entity
			spriteBatch.Begin(SpriteSortMode.BackToFront, null, null, null, null, null, ActiveCamera.GetMatrix());
			_activeEntities.ForEach(e => e.Draw(spriteBatch));
			spriteBatch.End();
		}

		/// <summary>
		/// Vrati tile na dane pozici v PX jednotkach.
		/// </summary>
		/// <returns>Tile.</returns>
		/// <param name="position">Pozice v PX jednotkach.</param>
		public Tile GetTileOnPosition_P(Vector2 position)
		{
			return GetTileOnPosition_T((int)position.X / Chunk.TileSize_PX, (int)position.Y / Chunk.TileSize_PX);
		}

		/// <summary>
		/// Vrati tile na dane pozici v T jednotkach.
		/// </summary>
		/// <returns>Tile.</returns>
		/// <param name="x">X koordinat v T jednotkach.</param>
		/// <param name="y">Y koordinat v T jednotkach.</param>
		public Tile GetTileOnPosition_T(int x, int y)
		{
			if (CurrentLocation == null || x < 0 || y < 0 
			    || x >= CurrentLocation.MapWidth_CH * Chunk.ChunkSize_T || y >= CurrentLocation.MapHeight_CH * Chunk.ChunkSize_T)
				return new Tile(); //Vratit prazdnou tile, pokud je pozice mimo tilemapu
			
			return CurrentLocation.Chunks[CurrentLocation.MapWidth_CH * (y / Chunk.ChunkSize_T) + x / Chunk.ChunkSize_T]
				.Tiles[Chunk.ChunkSize_T * (y % Chunk.ChunkSize_T) + x % Chunk.ChunkSize_T]; //Vrati Tile na dane pozici
		}

		/// <summary>
		/// Vrati tile na dane pozici v PX jednotkach.
		/// </summary>
		/// <returns>Tile.</returns>
		/// <param name="position">Pozice v PX jednotkach.</param>
		public void SetTileOnPosition_P(Vector2 position, byte tileset, UInt16 tileId, bool collision = false)
		{
			SetTileOnPosition_T((int)position.X / Chunk.TileSize_PX, (int)position.Y / Chunk.TileSize_PX, tileset, tileId, collision);
		}

		/// <summary>
		/// Vrati tile na dane pozici v T jednotkach.
		/// </summary>
		/// <returns>Tile.</returns>
		/// <param name="x">X koordinat v T jednotkach.</param>
		/// <param name="y">Y koordinat v T jednotkach.</param>
		public void SetTileOnPosition_T(int x, int y, byte tileset, UInt16 tileId, bool collision = false)
		{
			if (CurrentLocation == null || x < 0 || y < 0
				|| x >= CurrentLocation.MapWidth_CH * Chunk.ChunkSize_T || y >= CurrentLocation.MapHeight_CH * Chunk.ChunkSize_T)
				return;

			CurrentLocation.Chunks[CurrentLocation.MapWidth_CH * (y / Chunk.ChunkSize_T) + x / Chunk.ChunkSize_T]
						   .Tiles[Chunk.ChunkSize_T * (y % Chunk.ChunkSize_T) + x % Chunk.ChunkSize_T] = new Tile() { Tileset = tileset, ID = tileId, Collision = collision }; //Vrati Tile na dane pozici
		}
	}
}