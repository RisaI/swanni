﻿using System;
namespace swanni
{
	public struct Tile
	{
		public byte Tileset;
		public UInt16 ID;
		public bool Collision;

		public override bool Equals(object obj)
		{
			return base.Equals(obj);
		}

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		public static bool operator ==(Tile t0, Tile t1)
		{
			return t0.Tileset == t1.Tileset && t0.ID == t1.ID;
		}

		public static bool operator !=(Tile t0, Tile t1)
		{
			return t0.Tileset != t1.Tileset || t0.ID != t1.ID;
		}
	}
}
