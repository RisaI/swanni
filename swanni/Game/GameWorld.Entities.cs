﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace swanni.Game
{
	public partial class GameWorld //Entity
	{
		private List<Entity> _activeEntities; //Skutecny list aktivnich entit
		/// <summary>
		/// List entit.
		/// </summary>
		/// <value>Entity.</value>
		public IReadOnlyList<Entity> ActiveEntities
		{
			get;
			private set;
		}

		/// <summary>
		/// Vytvori prazdnou entitu a prida ji do kolekce aktivnich entit teto GameWorld instance.
		/// </summary>
		/// <returns>Nova entita.</returns>
		public Entity CreateEntity(Vector2 position)
		{
			return AddEntity(new Entity() { Position = position });
		}

		/// <summary>
		/// Prida existujici entitu do kolekce aktivnich entit teto GameWorld instance.
		/// </summary>
		/// <returns>Entita.</returns>
		/// <param name="ent">Entita.</param>
		public Entity AddEntity(Entity ent)
		{
			if (!_activeEntities.Contains(ent))
			{
				GlobalRegister.AddEntityToRegister(ent);
				_activeEntities.Add(ent);
				ent.OnSpawn(this);
			}
			return ent;
		}

		/// <summary>
		/// Vytvori entitu z assetu (nebo unikatni entitu z registru) a vlozi ji do seznamu aktivnich entit.
		/// </summary>
		/// <returns>Entita.</returns>
		/// <param name="asset">Asset.</param>
		/// <param name="position">Position.</param>
		public Entity SpawnEntity(string asset, Vector2 position)
		{
			Entity ent;
			if (!ContentLoader.IsEntityUnique(asset) || (ent = GlobalRegister.GetEntityFromRegister(asset)) == null)
				ent = ContentLoader.GetEntity(asset);
			ent.Position = position;
			return AddEntity(ent);
		}

		private List<Guid> _pendingRemove = new List<Guid>();
		/// <summary>
		/// Odstrani entitu ze seznamu aktivnich entit.
		/// </summary>
		/// <param name="uniqueTag">Unikatni tag entity.</param>
		public void RemoveActiveEntity(string uniqueTag)
		{
			var ent = GlobalRegister.GetEntityFromRegister(uniqueTag);
			if (ent != null)
				RemoveActiveEntity(ent);
		}

		/// <summary>
		/// Odstrani entitu ze seznamu aktivnich entit.
		/// </summary>
		/// <param name="ent">Entita.</param>
		public void RemoveActiveEntity(Entity ent)
		{
			RemoveActiveEntity(ent.UID);
		}

		/// <summary>
		/// Odstrani entitu ze seznamu aktivnich entit.
		/// </summary>
		/// <param name="uid">UID entity.</param>
		public void RemoveActiveEntity(Guid uid)
		{
			_pendingRemove.Add(uid);
		}

		/// <summary>
		/// Vrati vsechny entity ve ctvercove vzdalenosti od pozice (je to rychlejsi nez kruhova vzdalenost).
		/// </summary>
		/// <returns>Entity v dosahu.</returns>
		/// <param name="position">Pozice.</param>
		/// <param name="range">Vzdalenost.</param>
		public List<Entity> GetEntitiesInRange(Vector2 position, float range)
		{
			List<Entity> ents = new List<Entity>();
			_activeEntities.ForEach(e =>
			{
				if (Math.Abs(e.Position.X - position.X) <= range || Math.Abs(e.Position.Y - position.Y) <= range)
					ents.Add(e);
			});
			return ents;
		}
	}
}
