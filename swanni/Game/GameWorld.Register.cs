﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
namespace swanni.Game
{
	public partial class GameWorld
	{
		/// <summary>
		/// Persistentni registr unikatnich entit, dat o cestovani mezi lokacemi a globalnich dat.
		/// </summary>
		/// <value>Globalni registr.</value>
		public WorldRegister GlobalRegister
		{
			get;
			private set;
		}

		/// <summary>
		/// Zunikatni neunikatni entitu.
		/// </summary>
		/// <returns>Entita.</returns>
		/// <param name="ent">Entita.</param>
		public Entity MakeUnique(Entity ent)
		{
			if (!ent.Unique)
			{
				ent.MakeUnique(null);
				GlobalRegister.AddEntityToRegister(ent);
			}
			return ent;
		}

		public class WorldRegister : ISerializable
		{
			/// <summary>
			/// Dictionary globalnich dat.
			/// </summary>
			/// <value>Globalni data.</value>
			public Dictionary<string, string> GlobalData
			{
				get;
				private set;
			}

			/// <summary>
			/// Registr unikatnich Entit.
			/// </summary>
			/// <value>Registr entit.</value>
			public List<Entity> EntityRegister
			{
				get;
				private set;
			}

			/// <summary>
			/// Registr dat o cestovani entit mezi lokacemi.
			/// </summary>
			/// <value>Cestovatele.</value>
			public List<TravelInfo> Travellers
			{
				get;
				private set;
			}

			public WorldRegister()
			{
				EntityRegister = new List<Entity>();
				Travellers = new List<TravelInfo>();
				GlobalData = new Dictionary<string, string>();
			}

			/// <summary>
			/// Najde unikatni entitu v registru podle jejiho UID.
			/// </summary>
			/// <returns>Entita.</returns>
			/// <param name="uid">UID.</param>
			public Entity GetEntityFromRegister(Guid uid)
			{
				for (int i = 0; i < EntityRegister.Count; ++i)
				{
					if (EntityRegister[i].UID == uid)
						return EntityRegister[i];
				}
				return null;
			}

			/// <summary>
			/// Vrati unikatni entitu z registru podle jejiho unikatniho tagu.
			/// </summary>
			/// <returns>Entita.</returns>
			/// <param name="uniqueTag">Unikatni tag.</param>
			public Entity GetEntityFromRegister(string uniqueTag)
			{
				for (int i = 0; i < EntityRegister.Count; ++i)
				{
					if (EntityRegister[i].AssetName == uniqueTag)
						return EntityRegister[i];
				}
				return null;
			}

			/// <summary>
			/// Vlozi unikatni entitu do registru (musi mit unikatni tag!).
			/// </summary>
			/// <param name="ent">Entita.</param>
			public void AddEntityToRegister(Entity ent)
			{
				if (!EntityRegister.Contains(ent) && ent.Unique)
				{
					EntityRegister.Add(ent);
				}
			}

			/// <summary>
			/// Je entita soucasti registru?
			/// </summary>
			/// <returns><c>true</c>, pokud je entita v registru, <c>false</c> v opacnem pripade.</returns>
			/// <param name="ent">Entita.</param>
			public bool ContainsEntity(Entity ent)
			{
				return EntityRegister.Contains(ent);
			}

			/// <summary>
			/// Je entita soucasti registru?
			/// </summary>
			/// <returns><c>true</c>, pokud je entita v registru, <c>false</c> v opacnem pripade.</returns>
			/// <param name="uniqueTag">Unikatni tag entity.</param>
			public bool ContainsEntity(string uniqueTag)
			{
				return EntityRegister.Any(e => e.AssetName == uniqueTag);
			}

			public void Serialize(BinaryWriter writer)
			{
				writer.Write(EntityRegister.Count);
				for (int i = 0; i < EntityRegister.Count; ++i)
				{
					writer.Write(EntityRegister[i]);
				}
				writer.Write(Travellers.Count);
				for (int i = 0; i < Travellers.Count; ++i)
				{
					writer.Write(Travellers[i]);
				}
				writer.Write(GlobalData.Count);
				var globalDataList = GlobalData.ToList();
				for (int i = 0; i < GlobalData.Count; ++i)
				{
					writer.Write(globalDataList[i].Key);
					writer.Write(globalDataList[i].Value);
				}
			}

			public void Deserialize(BinaryReader reader)
			{
				EntityRegister.Clear();
				// Nacist entity
				{
					var count = reader.ReadInt32();
					for (int i = 0; i < count; ++i)
					{
						var ent = new Entity();
						ent.Deserialize(reader);
						EntityRegister.Add(ent);
					}
				}

				Travellers.Clear();
				// Nacist cestovatele
				{
					var count = reader.ReadInt32();
					for (int i = 0; i < count; ++i)
					{
						var tInfo = new TravelInfo();
						tInfo.Deserialize(reader);
						Travellers.Add(tInfo);
					}
				}

				GlobalData.Clear();
				// Nacist globalni data
				{
					var count = reader.ReadInt32();
					for (int i = 0; i < count; ++i)
					{
						GlobalData.Add(reader.ReadString(), reader.ReadString());
					}
				}
			}

			public struct TravelInfo : ISerializable
			{
				public string UniqueTag, Location, TravelNode;

				public TravelInfo(Entity ent, string location, string travelNode)
				{
					if (!ent.Unique)
						throw new Exception("");
					UniqueTag = ent.AssetName;
					Location = location;
					TravelNode = travelNode;
				}

				public void Serialize(BinaryWriter writer)
				{
					writer.Write(UniqueTag);
					writer.Write(Location);
					writer.Write(TravelNode);
				}

				public void Deserialize(BinaryReader reader)
				{
					UniqueTag = reader.ReadString();
					Location = reader.ReadString();
					TravelNode = reader.ReadString();
				}
			}
		}
	}
}