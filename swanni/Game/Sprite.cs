﻿using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
namespace swanni
{
	public class Sprite : ICloneable<Sprite>, IJsonSerializable
	{
		private string _textureAsset;

		/// <summary>
		/// Textura sprite.
		/// </summary>
		/// <value>Textura.</value>
		public Texture2D Texture
		{
			get;
			private set;
		}

		/// <summary>
		/// Registr animaci.
		/// </summary>
		private Dictionary<string, Animation> _animRegistry;

		/// <summary>
		/// Aktivni animace.
		/// </summary>
		private Animation _anim;

		/// <summary>
		/// Vrati nebo nastavi aktivni animaci.
		/// </summary>
		/// <value>Animace.</value>
		public Animation Animation
		{
			get { return _anim; }
			set
			{
				_anim = value; //Nastavit animaci
				_timer = 0; //Restartovat casovac
				CurrentFrame = _anim.HasFlag(Animation.StartReversed) ? _anim.EndFrame : _anim.StartFrame; //Ma zacit od konce?
				_negDir = _anim.HasFlag(Animation.StartReversed); //Ma jit opacnym smerem?
			}
		}

		/// <summary>
		/// Barva, kterou se sprite vykresluje.
		/// </summary>
		public Color Color = Color.White;

		/// <summary>
		/// Soucasny snimek.
		/// </summary>
		public int CurrentFrame = 0;

		/// <summary>
		/// Sprite efekt.
		/// </summary>
		public SpriteEffects Effect = SpriteEffects.None;

		/// <summary>
		/// Velikost sprite.
		/// </summary>
		public Vector2 Scale = Vector2.One;

		/// <summary>
		/// Ukotveni (origin v relativnich jednotkach, (0,0) je levy horni roh, (1,1) je pravy dolni roh).
		/// </summary>
		public Vector2 Anchor = new Vector2(0.5f);

		//Interni hodnoty.
		private int _frameWidth, _frameHeight, _totalFrames, _framesInRow;

		/// <summary>
		/// Konstruktor, ktery vytvori Sprite instanci z JSON objektu.
		/// </summary>
		/// <param name="obj">Objekt.</param>
		public Sprite(JObject obj)
		{
			ReadFromJson(obj);
		}

		/// <summary>
		/// Konstruktor sprite tridy, ktera slouzi k vykreslovani a animovani sprite sheetu.
		/// </summary>
		/// <param name="textureAsset">Asset nazev textury.</param>
		/// <param name="frameWidth">Sirka snimku.</param>
		/// <param name="frameHeight">Vyska snimku.</param>
		public Sprite(string textureAsset, int frameWidth, int frameHeight)
		{
			_textureAsset = textureAsset;
			_frameWidth = frameWidth;
			_frameHeight = frameHeight;
			Recalculate();
		}

		/// <summary>
		/// Konstruktor sprite tridy, ktera slouzi k vykreslovani a animovani sprite sheetu.
		/// </summary>
		/// <param name="textureAsset">Asset nazev textury.</param>
		/// <param name="frameWidth">Sirka snimku.</param>
		/// <param name="frameHeight">Vyska snimku.</param>
		/// <param name="anims">Registr animaci.</param>
		public Sprite(string textureAsset, int frameWidth, int frameHeight, Dictionary<string, Animation> anims)
			: this(textureAsset, frameWidth, frameHeight)
		{
			SetAnimationRegistry(anims);
		}


		/// <summary>
		/// Rekalkuluje runtime hodnoty.
		/// </summary>
		private void Recalculate()
		{
			Texture = ContentLoader.Textures[_textureAsset];
			_totalFrames = (_framesInRow = (Texture.Width / _frameWidth)) * (Texture.Height / _frameHeight);
		}

		/// <summary>
		/// Prepise registr animaci.
		/// </summary>
		/// <param name="anims">Registr animaci.</param>
		public void SetAnimationRegistry(Dictionary<string, Animation> anims)
		{
			_animRegistry = anims;
		}

		/// <summary>
		/// Vrati animaci z registru.
		/// </summary>
		/// <returns>Animace.</returns>
		/// <param name="name">Nazev animace.</param>
		public Animation GetAnimationFromRegistry(string name)
		{
			return _animRegistry[name];
		}

		/// <summary>
		/// Nastavi jako aktivni animaci z registru.
		/// </summary>
		/// <param name="name">Nazev animace.</param>
		public void PlayAnimationFromRegistry(string name, bool restartIfPlaying)
		{
			if (!_animRegistry.ContainsKey(name))
				return;
			var anim = _animRegistry[name];
			if (restartIfPlaying || _anim != anim)
				Animation = anim;
		}

		/// <summary>
		/// Zastavi animaci na soucasnem snimku.
		/// </summary>
		public void StopAnimation()
		{
			_anim.Interval = 0;
		}

		/// <summary>
		/// Source obdelnik v snimku v texture.
		/// </summary>
		public Rectangle SourceRectangle
		{
			get { return new Rectangle((CurrentFrame % _framesInRow) * _frameWidth, (CurrentFrame / _framesInRow) * _frameHeight, _frameWidth, _frameHeight); }
		}

		/// <summary>
		/// Jde animace opacnym smerem?
		/// </summary>
		private bool _negDir;

		/// <summary>
		/// Interni casovac.
		/// </summary>
		private float _timer;

		/// <summary>
		/// Provede update spritu, vypocita postup animace, atd.
		/// </summary>
		/// <param name="gameTime">Game time.</param>
		/// <param name="timeScale">Casova skala.</param>
		public void Update(GameTime gameTime, float timeScale)
		{
			if (Animation.Interval > 0 && Animation.StartFrame != Animation.EndFrame)
			{
				_timer += (float)gameTime.ElapsedGameTime.TotalSeconds * timeScale;

				if (_timer > Animation.Interval)
				{
					_timer %= Animation.Interval;
					if (_negDir) //Jde animace v opacnem smeru?
					{
						if (CurrentFrame > Animation.StartFrame)
						{
							--CurrentFrame; //Postoupit o snimek
						}
						else if (!Animation.HasFlag(Animation.DontRepeat)) //Pokud se muze opakovat
						{
							if (Animation.HasFlag(Animation.PingPong))
							{
								//Otocit prubeh animace, pokud je aktivni vlajka PingPong
								CurrentFrame = CurrentFrame + 1;
								_negDir = false;
							}
							else
								CurrentFrame = Animation.EndFrame; // Zacit animaci od konce
						}
					}
					else
					{
						if (CurrentFrame < Animation.EndFrame)
						{
							++CurrentFrame; //Postoupit o snimek
						}
						else if (!Animation.HasFlag(Animation.DontRepeat)) //Pokud se muze opakovat
						{
							if (Animation.HasFlag(Animation.PingPong))
							{
								//Otocit prubeh animace, pokud je aktivni vlajka PingPong
								CurrentFrame = CurrentFrame - 1;
								_negDir = true;
							}
							else
								CurrentFrame = Animation.StartFrame; //Zacit animaci od zacatku
						}
					}
				}
			}
		}

		/// <summary>
		/// Serializuje stav sprite instance.
		/// </summary>
		/// <param name="writer">Writer.</param>
		public void SerializeState(System.IO.BinaryWriter writer)
		{
			//Color, effect, scale, anchor
			writer.Write(Color);
			writer.Write((byte)Effect);
			writer.Write(Scale);
			writer.Write(Anchor);
			writer.Write(Animation);
			writer.Write(CurrentFrame);
			writer.Write(_negDir);
			writer.Write(_timer);
		}

		/// <summary>
		/// Deserializuje stav sprite instance.
		/// </summary>
		/// <param name="reader">Reader.</param>
		public void DeserializeState(System.IO.BinaryReader reader)
		{
			Color = reader.ReadColor();
			Effect = (SpriteEffects)reader.ReadByte();
			Scale = reader.ReadVector2();
			Anchor = reader.ReadVector2();
			var anim = new Animation();
			anim.Deserialize(reader);
			_anim = anim;
			CurrentFrame = reader.ReadInt32();
			_negDir = reader.ReadBoolean();
			_timer = reader.ReadSingle();
		}

		/// <summary>
		/// Zapise kompletni informace o Sprite do JSON formatu.
		/// </summary>
		/// <param name="writer">Writer.</param>
		public void WriteToJson(JsonWriter writer)
		{
			writer.WriteStartObject();
			writer.WritePropertyName("asset");
			writer.WriteValue(_textureAsset);
			writer.WritePropertyName("fwidth");
			writer.WriteValue(_frameWidth);
			writer.WritePropertyName("fheight");
			writer.WriteValue(_frameHeight);
			writer.WritePropertyName("scale");
			writer.WriteVector2(Scale);
			writer.WritePropertyName("anchor");
			writer.WriteVector2(Anchor);
			writer.WritePropertyName("color");
			writer.WriteColor(Color);
			writer.WritePropertyName("effect");
			writer.WriteValue(Effect.ToString());
			writer.WritePropertyName("animregistry");
			writer.WriteStartObject();
			_animRegistry.ToList().ForEach(pair =>
			{
				writer.WritePropertyName(pair.Key);
				pair.Value.WriteToJson(writer);
			});
			writer.WriteEndObject();
			writer.WriteEndObject();
		}

		/// <summary>
		/// Precte udaje o sprite z JSON formatu.
		/// </summary>
		/// <param name="obj">JObjekt.</param>
		public void ReadFromJson(JObject obj)
		{
			_textureAsset = (string)obj["asset"];
			_frameWidth = int.Parse((string)obj["fwidth"]);
			_frameHeight = int.Parse((string)obj["fheight"]);
			Recalculate();
			Scale = Helper.JTokenToVector2(obj["scale"]);
			Anchor = Helper.JTokenToVector2(obj["anchor"]);
			Color = Helper.JTokenToColor(obj["color"]);
			Effect = (SpriteEffects)Enum.Parse(typeof(SpriteEffects), (string)obj["effect"]);
			var registry = (JObject)obj["animregistry"];
			_animRegistry = new Dictionary<string, Animation>();
			foreach (JToken t in registry.Children())
			{
				if (t.Type == JTokenType.Property)
					_animRegistry.Add((t as JProperty).Name, Animation.FromJson((JObject)(t as JProperty).Value));
			}
		}

		/// <summary>
		/// Vykresli sprite v jeho soucasnem stavu.
		/// </summary>
		/// <param name="spriteBatch">Sprite batch.</param>
		/// <param name="position">Pozice.</param>
		/// <param name="depth">Hloubka.</param>
		public void Draw(SpriteBatch spriteBatch, Vector2 position, float depth)
		{
			spriteBatch.Draw(Texture, position, SourceRectangle, Color, 0f, new Vector2(_frameWidth * Anchor.X, _frameHeight * Anchor.Y), Scale, Effect, depth);
		}

		/// <summary>
		/// Vytvori klon Sprite instance.
		/// </summary>
		/// <returns>Klon.</returns>
		public Sprite Clone()
		{
			var s = new Sprite(_textureAsset, _frameWidth, _frameHeight)
			{
				_animRegistry = _animRegistry,
				Effect = Effect,
				Scale = Scale,
				Color = Color,
				CurrentFrame = CurrentFrame,
				Anchor = Anchor,
				Animation = _anim,
			};

			return s;
		}
	}

	/// <summary>
	/// Struct, ktery drzi informace o animaci.
	/// </summary>
	public struct Animation : ISerializable, IJsonSerializable
	{
		/// <summary>
		/// Vlajky atributu.
		/// </summary>
		public const byte DontRepeat = 1, PingPong = 2, StartReversed = 4;

		/// <summary>
		/// Pocatecni snimek.
		/// </summary>
		public byte StartFrame;

		/// <summary>
		/// Konecny snimek.
		/// </summary>
		public byte EndFrame;

		/// <summary>
		/// Obsahuje aktivni vlajky.
		/// </summary>
		public byte Data;

		/// <summary>
		/// Interval mezi skokem snimku.
		/// </summary>
		public float Interval;

		public Animation(byte startFrame, byte endFrame, float interval, byte data = 0)
		{
			StartFrame = startFrame;
			EndFrame = endFrame;
			Interval = interval;
			Data = data;
		}

		/// <summary>
		/// Zjisti, jestli je aktivni vlajka.
		/// </summary>
		/// <returns><c>true</c>, pokud je vlajka aktivni, <c>false</c> v opacnem pripade.</returns>
		/// <param name="flag">Vlajka.</param>
		public bool HasFlag(byte flag)
		{
			return (Data & flag) == flag;
		}

		public override bool Equals(object obj)
		{
			return base.Equals(obj);
		}

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		public void Serialize(System.IO.BinaryWriter writer)
		{
			writer.Write(StartFrame);
			writer.Write(EndFrame);
			writer.Write(Interval);
			writer.Write(Data);
		}

		public void Deserialize(System.IO.BinaryReader reader)
		{
			StartFrame = reader.ReadByte();
			EndFrame = reader.ReadByte();
			Interval = reader.ReadSingle();
			Data = reader.ReadByte();
		}

		public void WriteToJson(Newtonsoft.Json.JsonWriter writer)
		{
			writer.WriteStartObject();
			writer.WritePropertyName("startframe");
			writer.WriteValue(StartFrame);
			writer.WritePropertyName("endframe");
			writer.WriteValue(EndFrame);
			writer.WritePropertyName("interval");
			writer.WriteValue(Interval);
			writer.WritePropertyName("data");
			writer.WriteValue(Data);
			writer.WriteEndObject();
		}

		public void ReadFromJson(Newtonsoft.Json.Linq.JObject obj)
		{
			StartFrame = (byte)obj["startframe"];
			EndFrame = (byte)obj["endframe"];
			Interval = (float)obj["interval"];
			Data = (byte)obj["data"];
		}

		public static bool operator ==(Animation a, Animation b)
		{
			return a.StartFrame == b.StartFrame && a.EndFrame == b.EndFrame && a.Interval == b.Interval && a.Data == b.Data;
		}

		public static bool operator !=(Animation a, Animation b)
		{
			return a.StartFrame != b.StartFrame || a.EndFrame != b.EndFrame || a.Interval != b.Interval || a.Data != b.Data;
		}

		public static Animation FromJson(Newtonsoft.Json.Linq.JObject obj)
		{
			Animation anim = new Animation();
			anim.ReadFromJson(obj);
			return anim;
		}
	}
}
