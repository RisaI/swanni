﻿using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using System.Text;
using System.Text.RegularExpressions;

namespace swanni
{
	public class DeveloperConsole
	{
		/// <summary>
		/// Konstanta velikosti konzole.
		/// </summary>
		public const int HEIGHT = 328;
		const int SCROLL = 6;
		const float CURSOR_TIME = 0.5f, INPUT_TIME_F = 1f, INPUT_TIME = 0.05f;

		/// <summary>
		/// Reference na vlastnici Game1 tridu.
		/// </summary>
		/// <value>Game1.</value>
		public Game1 Game
		{
			get;
			private set;
		}

		public Game.GameWorld ControlledWorld;

		/// <summary>
		/// Font pouzity pri renderovani konzole.
		/// </summary>
		/// <value>SpriteFont.</value>
		public SpriteFont Font
		{
			get { return ContentLoader.Fonts["console"]; }
		}

		private int _off;

		/// <summary>
		/// Vraci offset od konce textu.
		/// </summary>
		/// <value>Offset.</value>
		public int Offset
		{
			get { return _off; }
			set
			{
				_off = MathHelper.Clamp(value, 0, Math.Max(0, Output.Count - 2));
			}
		}

		/// <summary>
		/// Predchozi zadany prikaz.
		/// </summary>
		/// <value>Predchozi zadany prikaz.</value>
		public string PreviousInput
		{
			get;
			private set;
		}

		private string _input;
		/// <summary>
		/// Momentalni zadavany text.
		/// </summary>
		/// <value>Zadavany text.</value>
		public string Input
		{
			get { return _input; }
			private set
			{
				_input = value;
				if (value != null)
					_cursorIndex = Math.Min(value.Length, _cursorIndex);
				else
					_cursorIndex = 0;
			}
		}

		/// <summary>
		/// Je konzole viditelna?
		/// </summary>
		public bool Enabled = false;

		protected Dictionary<string, ConsoleCommand> Commands = new Dictionary<string, ConsoleCommand>();

		/// <summary>
		/// Inicializuje instanci konzole.
		/// </summary>
		/// <param name="game">Game.</param>
		public DeveloperConsole(Game1 game)
		{
			Game = game;
			Game.Window.TextInput += (sender, args) =>
			{
				if (Enabled && args.Character != '`' && args.Character != '~' && Font.Characters.Contains(args.Character))
				{
					if (!string.IsNullOrEmpty(Input))
						Input = Input.Substring(0, Input.Length - _cursorIndex) + args.Character + Input.Substring(Input.Length - _cursorIndex);
					else
						Input = string.Empty + args.Character;

					RefreshCursor();
				}
			};
			Console.SetOut(new ConsoleWriter(this));

			AddCommand("help", "Shows the help menu.", args => // Help prikaz
			{
				if (args.Length == 0)
				{
					WriteLine(Game1.ProjectName + ", ver: " + Game1.Version);
					Commands.ToList().ForEach(pair =>
					{
						Console.WriteLine("{0} - {1} {2}", pair.Key, pair.Value.Description,
						                  pair.Value.ArgumentString != null ? "(args: " + pair.Value.ArgumentString + ")" : string.Empty);
					});
				}
				else
				{
					if (Commands.ContainsKey(args[0]))
					{
						var command = Commands[args[0]];
						Console.WriteLine("{0} - {1} {2}", args[0], command.Description,
						                  command.ArgumentString != null ? "(args: " + command.ArgumentString + ")" : string.Empty);
					}
					else
					{
						Console.WriteLine("Command '{0}' does not exist.", args[0]);
					}
				}
			}, "[command]");
			AddCommand("clear", "Clears the console output.", args =>
			{
				Output.Clear();
				Output.Add(string.Empty);
			});
			AddCommand("echo", "Prints every argument to the console.", args => // Echo prikaz
			{
				for (int i = 0; i < args.Length; ++i)
					WriteLine(args[i]);
			}, "strings");
			AddCommand("exit", "Quits the game.", args => // Exit prikaz
			{
				Game.Exit();
			});
			AddCommand("des", "Prints debug deserialization data.", args => // Deserialization prikaz
			{
				WriteLine(DeserializationRegister.DebugString());
			});
			AddCommand("save", "Saves the gameworld to a compressed file.", args => // Save prikaz
			{
				if (ControlledWorld != null)
				{
					ControlledWorld.SaveToFile(args[0]);
				}
			}, "filename");
			AddCommand("load", "Loads the game from a compressed file.", args => // Load prikaz
			{
				if (ControlledWorld != null)
				{
					ControlledWorld.LoadFromFile(args[0]);
				}
			}, "filename");

			#region Entity commands
			AddCommand("list_ents", "Lists all entities in GameWorld.", args => // List entit
			{
				int index = 0;
				ControlledWorld.ActiveEntities.ToList().ForEach(e =>
				{
					Console.WriteLine("Index: {0}, UID: {1}, Position: {2}", index++, e.UID, e.Position.ToString());
				});
			});
			AddCommand("ent_tojson", "Saves entity to a JSON file as a template", args => // Serializuje entitu do json souboru (jako sablonu pro budouci pouziti)
			{
				int index = int.Parse(args[0]);
				Helper.WriteToJsonFile(ControlledWorld.ActiveEntities[index], args[1]);
			}, "ent_index", "filename");
			AddCommand("ent_fromjson", "Loads entity from a JSON template.", args => // Nacte a spawne entitu z json souboru
			{
				Vector2 position = new Vector2(float.Parse(args[1]), float.Parse(args[2]));
				if (System.IO.File.Exists(args[0]))
				{
					using (System.IO.FileStream stream = new System.IO.FileStream(args[0], System.IO.FileMode.Open))
					using (System.IO.StreamReader sReader = new System.IO.StreamReader(stream))
					using (Newtonsoft.Json.JsonTextReader reader = new Newtonsoft.Json.JsonTextReader(sReader))
					{
						var ent = ControlledWorld.CreateEntity(position);
						ent.ReadFromJson((Newtonsoft.Json.Linq.JObject)Newtonsoft.Json.Linq.JObject.ReadFrom(reader));
					}
				}
			}, "filename", "x", "y");
			AddCommand("ent_spawn", "Spawns loaded entity from assets.", args =>
			{
				Vector2 pos = new Vector2(float.Parse(args[1]), float.Parse(args[2]));
				ControlledWorld.SpawnEntity(args[0], pos);
			}, "asset", "x", "y");
			#endregion

			#region Map commands
			AddCommand("map_clearchunks", "Clears the map with selected tile.", args =>
			{
				if (ControlledWorld.CurrentLocation != null)
				{
					byte tileSet = byte.Parse(args[0]);
					UInt16 tileId = UInt16.Parse(args[1]);
					for (int i = 0; i < ControlledWorld.CurrentLocation.Chunks.Length; ++i)
					{
						ControlledWorld.CurrentLocation.Chunks[i].Clear(tileSet, tileId);
					}
					Console.WriteLine("Cleared current location with tile [{0}; {1}].", tileSet, tileId);
				}
			}, "tileset", "tileId");
			AddCommand("map_savelocation", "Saves current location into two files (location info [json] and tilemap).", args =>
			{
				if (ControlledWorld.CurrentLocation != null)
				{
					ControlledWorld.CurrentLocation.SaveToFile(args[0]);
					WriteLine("Saved current location to a file.");
				}
			}, "filename");
			AddCommand("map_loadlocation", "Loads up a location from assets.", args =>
			{
				ControlledWorld.LoadLocation(args[0]);
			}, "asset");
			AddCommand("map_resize", "Resizes current tile map and copies overlaping chunks.", args =>
			{
				ControlledWorld.CurrentLocation.ResizeChunkMap(int.Parse(args[0]), int.Parse(args[1]));
			}, "width", "height");
			#endregion

			#region Editor
			AddCommand("editor", "Loads the editor.", args =>
			{
				Game.GameState = GameStateEnum.Editor;
			});
			AddCommand("editor_play","Sets editor to play/edit.", args =>
			{
				Editor.PLAYING = !Editor.PLAYING;
			});
			AddCommand("editor_tile", "Sets the current placed tile.", args =>
			{
				if (ControlledWorld.CurrentLocation != null)
				{
					byte tileSet = byte.Parse(args[0]);
					UInt16 tileId = UInt16.Parse(args[1]);
					bool collision = bool.Parse(args[2]);
					Editor.PLACING = new Tile() { Tileset = tileSet, ID = tileId, Collision = collision };
					Console.WriteLine("Current placed tile in editor set to: [{0}; {1}; {2}].", tileSet, tileId, collision);
				}
			}, "tileset", "tileId", "collision");
			#endregion

			#region Camera
			AddCommand("camera_pos", "Nastavi kameru", args =>
			{
				ControlledWorld.ActiveCamera.Position = new Vector2(float.Parse(args[0]), float.Parse(args[1]));
			}, "x", "y");
			#endregion
		}

		/// <summary>
		/// Prida prikaz do konzole.
		/// </summary>
		/// <param name="command">Nazev prikazu.</param>
		/// <param name="description">Popis prikazu.</param>
		/// <param name="argMinimum">Minimalni pocet argumentu.</param>
		/// <param name="action">Akce prikazu.</param>
		public void AddCommand(string command, string description, Action<string[]> action, params string[] arguments)
		{
			Commands.Add(command, new ConsoleCommand(description, action, arguments));
		}

		/// <summary>
		/// Update call konzole.
		/// </summary>
		/// <param name="gameTime">Game time.</param>
		public void Update(GameTime gameTime)
		{
			_cursorTimer += (float)gameTime.ElapsedGameTime.TotalSeconds;
			if (_cursorTimer >= CURSOR_TIME)
			{
				_cursorTimer %= CURSOR_TIME;
				_cursorVisible = !_cursorVisible;
			}

			if (_inputTimer > 0)
			{
				if (swanni.Input.KBState.IsKeyDown(Keys.Back) ||
					swanni.Input.KBState.IsKeyDown(Keys.Delete) ||
					swanni.Input.KBState.IsKeyDown(Keys.Left) ||
					swanni.Input.KBState.IsKeyDown(Keys.Right))
					_inputTimer -= (float)gameTime.ElapsedGameTime.TotalSeconds;
				else
					_inputTimer = 0;
			}

			// Predchozi vstup
			if (swanni.Input.KeyPressed(Keys.Up) && !string.IsNullOrWhiteSpace(PreviousInput))
			{
				Input = PreviousInput; // Predchozi input pri stisknuti sipky nahoru
			}
			if (Input != null)
			{
				if (swanni.Input.KeyPressed(Keys.Enter)) // Spustit prikaz pri stisknuti Enteru
				{
					Input = Input.Trim();

					if (!string.IsNullOrWhiteSpace(Input))
					{
						string command = Input.Split(' ')[0],
						remainder = Input.Substring(command.Length).Trim();
						Regex reg = new Regex(@"(\""[^\""]*\"")|(\b\S+\b)");
						var matches = reg.Matches(remainder);
						string[] args = new string[matches.Count];
						for (int i = 0; i < matches.Count; ++i)
						{
							args[i] = matches[i].Value.Replace("\"", string.Empty); // zpracovat argumenty
						}
						Command(command, args); // Spustit prikaz
						PreviousInput = Input;
					}

					Input = null;
				}
				else
				{
					// Backspace a delete
					if (swanni.Input.KBState.IsKeyDown(Keys.Back) && Input.Length - _cursorIndex > 0 && _inputTimer <= 0)
					{
						_inputTimer += swanni.Input.KeyPressed(Keys.Back) ? INPUT_TIME_F : INPUT_TIME;
						if (_cursorIndex == 0)
							Input = Input.Substring(0, Input.Length - 1); // Smazat jeden znak
						else
							Input = Input.Substring(0, Input.Length - 1 - _cursorIndex) + Input.Substring(Input.Length - _cursorIndex);
						
						RefreshCursor();
					}
					else if (swanni.Input.KBState.IsKeyDown(Keys.Delete) && _cursorIndex > 0 && _inputTimer <= 0)
					{
						_inputTimer += swanni.Input.KeyPressed(Keys.Delete) ? INPUT_TIME_F : INPUT_TIME;
						Input = Input.Substring(0, Input.Length - _cursorIndex) + Input.Substring(Input.Length - _cursorIndex + 1);
						if (Input.Length != _cursorIndex)
							--_cursorIndex;
						RefreshCursor();
					}

					// Navigace kurzoru
					if (swanni.Input.KBState.IsKeyDown(Keys.Left) && _inputTimer <= 0)
					{
						_inputTimer += swanni.Input.KeyPressed(Keys.Left) ? INPUT_TIME_F : INPUT_TIME;
						_cursorIndex = MathHelper.Min(Input.Length, _cursorIndex + 1);
						RefreshCursor();
					}
					if (swanni.Input.KBState.IsKeyDown(Keys.Right) && _inputTimer <= 0)
					{
						_inputTimer += swanni.Input.KeyPressed(Keys.Right) ? INPUT_TIME_F : INPUT_TIME;
						_cursorIndex = MathHelper.Max(0, _cursorIndex - 1);
						RefreshCursor();
					}
				}

				if (swanni.Input.KeyPressed(Keys.End))
				{
					_cursorIndex = 0; // Vrati kurzor na konec inputu
									  // Offset = 0; // Odstranit scrollovani
				}
				else if (swanni.Input.KeyPressed(Keys.Home))
				{
					_cursorIndex = Input.Length; // vrati kurzor na zacatek inputu.
												 // Offset = int.MaxValue; // Maximalni scroll
				}

				if (swanni.Input.KeyPressed(Keys.Escape))
				{
					Input = null; // Smazat cely Input
				}

			}

			// Scrollovani
			if (swanni.Input.KeyPressed(Keys.PageUp))
			{
				Offset += SCROLL; // Scroll nahoru
			}
			else if (swanni.Input.KeyPressed(Keys.PageDown))
			{
				Offset -= SCROLL; // Scroll dolu
			}

			// Autocomplete
			if (swanni.Input.KeyPressed(Keys.Tab) && !string.IsNullOrEmpty(Input))
			{
				Input = Input.TrimStart();
				if (!string.IsNullOrEmpty(Input))
				{
					var commands = Commands.Keys.ToList().FindAll(key => key.StartsWith(Input.ToLower(), StringComparison.InvariantCulture));
					if (commands.Count == 1)
						Input = commands[0] + " ";
					else if (commands.Count > 1)
					{
						string common = Input.ToLower();
						int shortest = int.MaxValue;
						commands.ForEach(c => { if (c.Length < shortest) { shortest = c.Length; } });
						for (int iCh = common.Length; iCh < shortest; ++iCh)
						{
							bool contains = true;
							for (int i = 1; i < commands.Count; ++i)
							{
								if (commands[i][iCh] != commands[0][iCh])
								{
									contains = false;
									break;
								}
							}
							if (contains)
								common += commands[0][iCh];
							else
								break;
						}
						if (common.Length > Input.Length)
							Input = common;
						else
						{
							string output = string.Empty;
							commands.ForEach(c => { output += c + ", "; });
							output = output.Substring(0, output.Length - 2);
							if (Output.Count <= 1 || Output[Output.Count - 2] != output)
								WriteLine(output);
						}
					}
				}
			}

			// Enable/disable
			if (swanni.Input.KeyPressed(Keys.OemTilde))
			{
				Enabled = !Enabled; // Vypnout/zapnout konzoli pri stisknuti tilde (pod escapem)
			}
		}

		private bool _cursorVisible = true;
		private float _cursorTimer = 0, _inputTimer = 0;
		private int _cursorIndex = 0;

		/// <summary>
		/// Draw call konzole.
		/// </summary>
		/// <param name="spriteBatch">Sprite batch.</param>
		public void Draw(SpriteBatch spriteBatch)
		{
			int cWidth = (int)Game.Resolution.X;
			spriteBatch.GraphicsDevice.ScissorRectangle = new Rectangle(0, 0, cWidth, HEIGHT);
			spriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.AlphaBlend, null, null, new RasterizerState() { ScissorTestEnable = true });
			spriteBatch.DrawRectangle(new Rectangle(0, 0, cWidth, HEIGHT), new Color(0, 0, 0, 200), 0f);

			// Vykresleni outputu
			if (Output.Count > 1 || !string.IsNullOrEmpty(Output[0]))
			{
				int lastModifier = string.IsNullOrWhiteSpace(Output[Output.Count - 1]) ? 1 : 0; // Pokud je posledni output prazdny, vynechat ho z renderu.
				int drawRange = MathHelper.Min(HEIGHT / Font.LineSpacing + 1, Output.Count - lastModifier);
				int currentOffset = 0;
				for (int i = Offset + lastModifier; i < Math.Min(Output.Count, drawRange + Offset + lastModifier); ++i)
				{
					var line = Output[Output.Count - 1 - i];
					if (!string.IsNullOrWhiteSpace(line))
					{
						currentOffset += ((int)Font.MeasureString(line).X - 1) / cWidth + 1;
						int localCounter = 1, standardOffset = (Offset % HEIGHT);
						for (int s = 0; s < line.Length; ++s)
						{
							if (Font.MeasureString(line.Substring(0, s)).X > cWidth)
							{
								spriteBatch.DrawString(Font, line.Substring(0, s - 1), new Vector2(0, HEIGHT - (currentOffset - localCounter) * Font.LineSpacing), Color.White,
													   0, new Vector2(0, Font.LineSpacing), 1, SpriteEffects.None, 1f);
								++localCounter;
								line = line.Substring(s - 1);
								s = 1;
							}
							else if (s == line.Length - 1)
							{
								spriteBatch.DrawString(Font, line, new Vector2(0, HEIGHT - (currentOffset - localCounter) * Font.LineSpacing), Color.White,
													   0, new Vector2(0, Font.LineSpacing), 1, SpriteEffects.None, 1f);
								break;
							}
						}
					}
					else
					{
						++currentOffset;
					}
				}

			}
			spriteBatch.End();

			spriteBatch.GraphicsDevice.ScissorRectangle = new Rectangle(0, HEIGHT, cWidth, Font.LineSpacing);
			spriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.AlphaBlend, null, null, new RasterizerState() { ScissorTestEnable = true });
			spriteBatch.DrawRectangle(new Rectangle(0, HEIGHT, cWidth, Font.LineSpacing), new Color(0, 0, 0, 225), 0f);
			{
				if (!string.IsNullOrEmpty(Input))
					spriteBatch.DrawString(Font, Input, new Vector2(0, HEIGHT), Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);
				if (_cursorVisible)
				{
					float x = string.IsNullOrEmpty(Input) ? 0 : Font.MeasureString(Input.Substring(0, Input.Length - _cursorIndex)).X;
					spriteBatch.Draw(ContentLoader.Pixel, new Vector2(x, HEIGHT), null, Color.White, 0, Vector2.Zero,
					                 new Vector2(1, Font.LineSpacing), SpriteEffects.None, 1f);
				}
			}
			spriteBatch.End();
		}

		/// <summary>
		/// Spusti dany prikaz s danymi argumenty
		/// </summary>
		/// <returns>The command.</returns>
		/// <param name="command">Command.</param>
		/// <param name="args">Arguments.</param>
		public void Command(string command, string[] args)
		{
			if (Commands.ContainsKey(command))
			{
				try
				{
					Commands[command].Invoke(args);
				}
				catch (Exception ex)
				{
					Console.WriteLine("An error has occured while executing command. Message: {0}", ex.Message);
				}
			}
			else
				Console.WriteLine("Command '{0}' does not exist.", command);
		}

		/// <summary>
		/// Drzi cely vystup konzole.
		/// </summary>
		List<string> Output = new List<string>(2048) { "Initializing console.", string.Empty };

		public void WriteLine()
		{
			Output.Add(string.Empty);
		}

		/// <summary>
		/// Napise radek do konzole.
		/// </summary>
		/// <param name="line">Radek.</param>
		public void WriteLine(string line)
		{
			if (line.Contains('\n'))
			{
				Output[Output.Count - 1] += line.Substring(0, line.IndexOf('\n'));
				line = line.Substring(line.IndexOf('\n') + 1);
				foreach (string subline in line.Split('\n'))
				{
					Output.Add(subline);
				}
			}
			else
			{
				Output[Output.Count - 1] += line;
			}
			Output.Add(string.Empty);
		}

		/// <summary>
		/// Napise znak do aktualniho nejnovejsiho radku konzole.
		/// </summary>
		/// <param name="value">Hodnota.</param>
		public void Write(char value)
		{
			if (value == '\n')
				Output.Add(string.Empty);
			else
				Output[Output.Count - 1] += value;
		}

		/// <summary>
		/// Zobrazi kurzor a vynuluje casovac.
		/// </summary>
		public void RefreshCursor()
		{
			_cursorTimer = 0;
			_cursorVisible = true;
		}

		public class ConsoleCommand
		{
			private Action<string[]> _action;
			private int _minimumArguments = 0;

			public string[] Arguments
			{
				get;
				private set;
			}

			public string ArgumentString
			{
				get;
				private set;
			}

			public string Description
			{
				get;
				private set;
			}

			public ConsoleCommand(string description, Action<string[]> action, string[] arguments)
			{
				Description = description;
				_action = action;
				Arguments = arguments;
				if (Arguments != null && Arguments.Length > 0)
				{
					ArgumentString = string.Empty;
					for (int i = 0; i < arguments.Length; ++i)
					{
						ArgumentString += Arguments[i];
						if (i < Arguments.Length - 1)
							ArgumentString += ", ";

						if (_minimumArguments == 0 && arguments[i].StartsWith("[", StringComparison.InvariantCulture)
							&& arguments[i].EndsWith("]", StringComparison.InvariantCulture))
						{
							_minimumArguments = i;
						}
					}
				}
			}

			public void Invoke(string[] args)
			{
				if (args.Length < _minimumArguments)
					Console.WriteLine("Invalid parameters, check help for this command.");
				else if (_action != null)
					_action.Invoke(args);
			}
		}

		/// <summary>
		/// Trida zajistujici prepis z System.Console do DeveloperConsole
		/// </summary>
		class ConsoleWriter : System.IO.TextWriter
		{
			public DeveloperConsole Console
			{
				get;
				private set;
			}

			public ConsoleWriter(DeveloperConsole console)
			{
				Console = console;
			}

			public override void Write(char value)
			{
				Console.Write(value);
			}

			public override Encoding Encoding
			{
				get
				{
					return Encoding.UTF8;
				}
			}
		}
	}
}