﻿using System;
namespace swanni
{
	public interface ICloneable<T>
	{
		T Clone();
	}
}
