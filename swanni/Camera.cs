﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
namespace swanni
{
	public class Camera
	{
		public Vector2 Position;

		private float _zoom = 1f;
		public float Zoom
		{
			get { return _zoom; }
			set
			{
				_zoom = MathHelper.Max(0.1f, value);
			}
		}

		private GraphicsDevice Graphics;

		public Camera(GraphicsDevice graphics)
		{
			Graphics = graphics;
		}

		/// <summary>
		/// Vypocita a vrati translaci kamery.
		/// </summary>
		/// <returns>Matice.</returns>
		public Matrix GetMatrix()
		{
			return Matrix.CreateScale(_zoom, _zoom, 1) *
				         Matrix.CreateTranslation(Graphics.Viewport.Width / 2 - Position.X, Graphics.Viewport.Height / 2 - Position.Y, 1);
		}

		/// <summary>
		/// Vrati prostor, ktery kamera snima.
		/// </summary>
		/// <value>Plocha.</value>
		public Rectangle Area
		{
			get 
			{
				var size = new Vector2(Graphics.Viewport.Width, Graphics.Viewport.Height) / _zoom;
				return new Rectangle((int)(Position.X - size.X / 2), (int)(Position.Y - size.Y / 2), (int)size.X, (int)size.Y); 
			}
		}

		/// <summary>
		/// Konvertuje svetove souradnice na obrazovkove souradnice.
		/// </summary>
		/// <returns>Obrazovkove souradnice.</returns>
		/// <param name="world">Svetove souradnice.</param>
		public Vector2 WorldToScreen(Vector2 world)
		{
			return Vector2.Transform(world, GetMatrix());
		}

		/// <summary>
		/// Konvertuje souradnice z obrazovkovych souradnic na svetove souradnice.
		/// </summary>
		/// <returns>Pozice v hernim svere.</returns>
		/// <param name="screen">Obrazovkove souradnice.</param>
		public Vector2 ScreenToWorld(Vector2 screen)
		{
			return Vector2.Transform(screen, Matrix.Invert(GetMatrix()));
		}

		/// <summary>
		/// Vrati souradnici vrchni strany viditelneho prostoru.
		/// </summary>
		/// <value>Vrchni souradnice.</value>
		public float Top
		{
			get { return Position.Y - Graphics.Viewport.Height / (2 * _zoom); }
		}

		/// <summary>
		/// Vrati souradnici leve strany viditelneho prostoru.
		/// </summary>
		/// <value>Leve souradnice.</value>
		public float Left
		{
			get { return Position.X - Graphics.Viewport.Width / (2 * _zoom); }
		}

		/// <summary>
		/// Vrati souradnici spodni strany viditelneho prostoru.
		/// </summary>
		/// <value>Spodni souradnice.</value>
		public float Bottom
		{
			get { return Position.Y + Graphics.Viewport.Height / (2 * _zoom); }
		}

		/// <summary>
		/// Vrati souradnici prave strany viditelneho prostoru.
		/// </summary>
		/// <value>Prave souradnice.</value>
		public float Right
		{
			get { return Position.X + Graphics.Viewport.Width / (2 * _zoom); }
		}

		public float GetDepth(Vector2 position)
		{
			return -MathHelper.Clamp((position.Y - Top) / ((Graphics.Viewport.Height + 256) / _zoom), 0f, 1f);
		}
	}
}
